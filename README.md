# Sistem Kelola Tesis

## Project using Laravel

### Runing and Deploy from Source

1. Setup *xampp* or *apache2*. (*apache2* mush direct (point) to `public` directory).
2. Setup database (user and database).
3. Run command `php artisan migrate`.
4. Run command `cp .env.example .env`. (From Linux - you may just copy the file and renamed it).
5. Edit the `.env` same with you configuration in database and xampp.
6. Run command `php artisan db:seed`. (For first time seed) or `php artisan migrate:refresh --seed` (for already seed).
7. Run command `php artisan key:generate`. (Generate key to ready publish)

# Contributor

PPL Kelompok D

1. Ahmad Rizdaputra - 13513027
2. Jeremia Jason Lasiman - 13514021
3. Bervianto Leo P - 13514047
4. Sekar Angila Hapsari - 13514069
5. Ahmad Faiq Rahman - 13514081
6. Resa Kemal Saharso - 13514109