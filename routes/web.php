<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index');
Route::post('/approve-mahasiswa', ['uses' => 'HomeController@approveMhs']);
Route::post('/approve-topik', ['uses' => 'HomeController@approveTopik']);
Route::post('/deleteReqDos', ['uses' => 'HomeController@deleteReqDos']);

//Edit Parameter Penilaian
Route::get('/daftarparameter', 'ParameterController@daftar');
Route::get('/newversion', 'ParameterController@newversion');
Route::get('/editparameter/{tipe}/{id}', 'ParameterController@load');
Route::post('/editparameter/submit', 'ParameterController@edit');
Route::post('/editparameter/delete', 'ParameterController@delete');

//List Seminar (Admin)
Route::get('/listseminar', 'SeminarController@seminarlist');
Route::get('/changestate/{id}', ['uses' => 'SeminarController@change']);
Route::get('/seminardetail/{id}', ['as' => 'seminardetail', 'uses' => 'SeminarController@detail']);
Route::post('/seminardetail/update', 'SeminarController@detailupdate');

//List Seminar (Dosen)
Route::get('/seminar', 'SeminarController@available');

//Sesi Seminar
Route::get('/addsession', 'SeminarController@addseminarsession');
Route::post('/addsession/submit', 'SeminarController@add');
Route::get('/seminarsession/{id}', ['as' => 'seminarsession', 'uses' => 'SeminarController@show']);
Route::post('/seminarsession/update', 'SeminarController@update');


Route::post('/addDosenPembimbing', ['uses' => 'HomeController@addDosenPembimbing']);
Route::post('/editTopik', ['uses' => 'HomeController@editTopik']);

//GET Method  - Bimbingan
Route::get('/bimbingan', ['uses' => 'BimbinganController@index']);
Route::get('/bimbingan/tulis', ['uses' => 'BimbinganController@create']);
Route::get('/bimbingan/{id}', ['uses' => 'BimbinganController@detail']);
Route::get('/bimbingan/mahasiswa/{id}', ['uses' => 'BimbinganController@mahasiswaBimbingan']);

//POST Method  - Bimbingan
Route::post('/bimbingan', ['uses' => 'BimbinganController@store']);
Route::post('/approve-bimbingan', ['uses' => 'BimbinganController@approve']);

//GET - list (daftar mahasiswa / dosen)
Route::get('/mahasiswa', ['uses' => 'ListController@listMahasiswa']);
Route::get('/mahasiswa/{id}', ['uses' => 'ListController@mahasiswaDetail']);
Route::get('/dosen', ['uses' => 'ListController@listDosen']);
Route::get('/dosen/{id}', ['uses' => 'ListController@dosenDetail']);


Route::get('/jadwal', ['uses' => 'JadwalController@index']);
Route::post('/jadwal/jadwalSpec', ['uses' => 'JadwalController@retMonth']);
Route::get('/jadwal/jadwalBaru', ['uses' => 'JadwalController@create']);
Route::post('/jadwal', ['uses' => 'JadwalController@store']);
Route::get('/jadwal/edit/{id}', ['uses' => 'JadwalController@edit']);
Route::get('/jadwal/edited/{id}', ['uses' => 'JadwalController@updateData']);
Route::get('/jadwal/delete/{id}', ['uses' => 'JadwalController@deleteData']);
Route::get('/jadwal/ajukan/{id}', ['uses' => 'JadwalController@pengajuanJadwal']);
Route::get('/jadwal/seminarsidang/edit/{id}', ['uses' => 'JadwalController@editSeminarSidang']);
Route::get('/jadwal/seminarsidang', ['uses' => 'JadwalController@updateSeminarSidang']);
Route::get('/jadwal/approve', ['uses' => 'JadwalController@approve']);
Route::post('/jadwal/confirmApproval', ['uses' => 'JadwalController@confirmApproval']);
Route::post('/jadwal/postApproval', ['uses' => 'JadwalController@postApproval']);
Route::post('/jadwal/decline', ['uses' => 'JadwalController@decline']);
Route::get('/jadwal/view', ['uses' => 'JadwalController@view']);

// Profile
Route::get('/show-profile/', ['uses' => 'ProfileController@showProfile']);
Route::get('/edit-profile/', ['uses' => 'ProfileController@editProfile']);
Route::post('/update-profile/', ['uses' => 'ProfileController@updateProfile']);

// Account Management
Route::get('/add-account/', ['uses' => 'AccountController@index']);
Route::post('/add-account/mahasiswa', ['uses' => 'AccountController@addMahasiswa']);
Route::post('/add-account/dosen', ['uses' => 'AccountController@addDosen']);

// Help Page
Route::get('/help/', ['uses' => 'HelpController@index']);
