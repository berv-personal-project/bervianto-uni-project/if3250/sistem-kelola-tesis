<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePengajuanJadwalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pengajuan_jadwal', function (Blueprint $table) {
            $table->increments('ID');
            $table->integer('jadwal_id')->unsigned();
            $table->foreign('jadwal_id')->references('ID')->on('jadwal')->onDelete('cascade');
            $table->Date('Tanggal');
            $table->Time('Waktu_Awal');
            $table->Time('Waktu_Akhir');
            $table->tinyInteger('Jenis_Kegiatan');
            $table->string('Keterangan')->nullable();
            $table->string('MahasiswaID');
            $table->index('MahasiswaID');
            $table->tinyInteger('Approved');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pengajuan_jadwal');
    }
}
