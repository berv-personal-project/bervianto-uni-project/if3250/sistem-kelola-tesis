<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeminarNilaiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seminar_nilai', function (Blueprint $table) {
            $table->integer('SeminarID');
            $table->string('Tipe');
            $table->string('DosenID');
            $table->string('ParameterID');
            $table->tinyInteger('ParamValue');
            $table->primary(array('SeminarID','Tipe','DosenID','ParameterID'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('seminar_nilai');
    }
}
