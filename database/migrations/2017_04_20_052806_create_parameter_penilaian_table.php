<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParameterPenilaianTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parameter_penilaian', function (Blueprint $table) {
            $table->string('Tipe');
            $table->string('ParameterID');
            $table->string('NamaParameter');
            $table->string('Deskripsi_L')->nullable();
            $table->string('Deskripsi_M')->nullable();
            $table->string('Deskripsi_K')->nullable();
            $table->integer('ChildSum');
            $table->integer('Level');
            $table->string('Parent')->nullable();
            $table->primary(array('Tipe', 'ParameterID'));
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parameter_penilaian');
    }
}
