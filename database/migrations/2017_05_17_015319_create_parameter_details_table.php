<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParameterDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parameter_detail', function (Blueprint $table) {
            $table->string('Tipe');
            $table->string('ParameterID');
            $table->string('Kriteria');
            $table->string('HasilParameter');
            $table->primary(array('Tipe', 'ParameterID','Kriteria'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parameter_detail');
    }
}
