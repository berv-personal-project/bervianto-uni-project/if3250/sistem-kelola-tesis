<?php


use App\Models\Dosen;
use App\Models\KategoriSeminarSidang;
use App\Models\Mahasiswa;
use App\Models\MahasiswaDosbing;
use App\Models\ParameterPenilaian;
use App\Models\ParameterOption;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use League\Csv\Reader;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(MahasiswaTableSeeder::class);
        $this->command->info('Mahasiswa Table seeded!');
        $this->call(DosenTableSeeder::class);
        $this->command->info('Dosen Table seeded!');
        $this->call(UserTableSeeder::class);
        $this->command->info('Admin and Kaprodi seeded!');
        $this->call(InitialiationSeeder::class);
        $this->command->info("Initialitaion");
        $this->call(KategoriSeminarSidangTableSeeder::class);
        $this->command->info('Kategori sidang seeded!');
        $this->call(ParameterPenilaianTableSeeder::class);
        $this->command->info('Parameter penilaian seeded!');
        $this->call(ParameterOptionTableSeeder::class);
        $this->command->info('Parameter option seeded!');
        $this->call(JadwalTableSeeder::class);
        $this->command->info('Jadwal Table seeded!');
    }
}

class MahasiswaTableSeeder extends Seeder
{
    public function run()
    {
        //load the CSV document from a stream
        $csv = Reader::createFromPath(base_path() . '/database/seeds/csvs/IF6099-01.csv', 'r');
        $csv->setDelimiter(';');
        $csv->setHeaderOffset(0);

        foreach ($csv as $row) {
            $user = User::where('username', $row["NIM"])->first();
            if ($user == null) {
                $t = time();
                DB::table('users')->insert([
                    'name' => $row["Nama"],
                    'username' => $row["NIM"],
                    'password' => bcrypt('test123'),
                    'role' => "Mahasiswa",
                    'created_at' => date("Y-m-d H:i:s", $t),
                    'updated_at' => date("Y-m-d H:i:s", $t),
                ]);
            }
        }
    }
}

class DosenTableSeeder extends Seeder
{
    public function run()
    {
        //load the CSV document from a stream
        $csv = Reader::createFromPath(base_path() . '/database/seeds/csvs/IF6099-01-Dosen.csv', 'r');
        $csv->setDelimiter(';');
        $csv->setHeaderOffset(0);

        foreach ($csv as $row) {
            $user = User::where('username', $row["NIP"])->first();
            if ($user == null) {
                $t = time();
                DB::table('users')->insert([
                    'name' => $row["Nama"],
                    'username' => $row["NIP"],
                    'password' => bcrypt('test123'),
                    'role' => "Dosen",
                    'created_at' => date("Y-m-d H:i:s", $t),
                    'updated_at' => date("Y-m-d H:i:s", $t),
                ]);
            }
        }
    }
}

class UserTableSeeder extends Seeder
{
    public function run()
    {
        $t = time();
        $admin = User::where('username', 'admin')->first();
        $kaprodi = User::where('username', 'kaprodi')->first();
        if ($admin == null) {
            DB::table('users')->insert([
                'name' => "Administrator",
                'username' => "admin",
                'password' => bcrypt('test123'),
                'role' => "Admin",
                'created_at' => date("Y-m-d H:i:s", $t),
                'updated_at' => date("Y-m-d H:i:s", $t),
            ]);
        }
        if ($kaprodi == null) {
            DB::table('users')->insert([
                'name' => "Bayu Hendrajaya",
                'username' => "kaprodi",
                'password' => bcrypt('test123'),
                'role' => "Kaprodi",
                'created_at' => date("Y-m-d H:i:s", $t),
                'updated_at' => date("Y-m-d H:i:s", $t),
            ]);
        }
    }
}

class KategoriSeminarSidangTableSeeder extends Seeder
{
    public function run()
    {
        $category = KategoriSeminarSidang::all();

        if (count($category) == 0) {
            for ($i = 1; $i <= 5; $i++) {
                DB::table('kategori_seminar_sidang')->insert([
                    'Jenis' => 'Seminar ' . $i,
                    'Deskripsi' => 'Kegiatan seminar tahap ' . $i,
                ]);
            }

            for ($i = 1; $i <= 5; $i++) {
                DB::table('kategori_seminar_sidang')->insert([
                    'Jenis' => 'Sidang ' . $i,
                    'Deskripsi' => 'Kegiatan sidang tahap ' . $i,
                ]);
            }
        }
    }

}

class ParameterPenilaianTableSeeder extends Seeder
{
    public function run()
    {
        $parameter = ParameterPenilaian::all();

        if (count($parameter) == 0) {
            //$this->template();
            $this->data();
        }
    }

    public function template()
    {
        //Seminar Proposal
            //Parameter Pembimbing
            DB::table('parameter_penilaian')->insert([
                'Tipe' => "1",
                'ParameterID' => "-",
                'NamaParameter' => "Rekapitulasi Akhir",
                'Level' => -2,
                'ChildSum' => 2,
            ]);
            DB::table('parameter_penilaian')->insert([
                'Tipe' => "1_0",
                'ParameterID' => "0",
                'NamaParameter' => "Nilai Pembimbing",
                'Level' => 0,
                'ChildSum' => 1,
                'Parent' => '-',
            ]);
            DB::table('parameter_penilaian')->insert([
                'Tipe' => "1_0",
                'ParameterID' => "1",
                'NamaParameter' => "Template Parameter",
                'Level' => 1,
                'ChildSum' => 0,
                'Parent' => '0',
            ]);

            //Parameter Penguji
            DB::table('parameter_penilaian')->insert([
                'Tipe' => "1_1",
                'ParameterID' => "-1",
                'NamaParameter' => "Rekapitulasi Nilai Penguji",
                'Level' => -1,
                'ChildSum' => 2,
                'Parent' => '-',
            ]);
            DB::table('parameter_penilaian')->insert([
                'Tipe' => "1_1",
                'ParameterID' => "0",
                'NamaParameter' => "Nilai Penguji",
                'Level' => 0,
                'ChildSum' => 1,
                'Parent' => '-1',
            ]);
            DB::table('parameter_penilaian')->insert([
                'Tipe' => "1_1",
                'ParameterID' => "1",
                'NamaParameter' => "Template Parameter",
                'Level' => 1,
                'ChildSum' => 0,
                'Parent' => '0',
            ]);

        //Seminar
            //Parameter Pembimbing
            DB::table('parameter_penilaian')->insert([
                'Tipe' => "2",
                'ParameterID' => "-",
                'NamaParameter' => "Rekapitulasi Akhir",
                'Level' => -2,
                'ChildSum' => 2,
            ]);
            DB::table('parameter_penilaian')->insert([
                'Tipe' => "2_0",
                'ParameterID' => "0",
                'NamaParameter' => "Nilai Pembimbing",
                'Level' => 0,
                'ChildSum' => 1,
                'Parent' => '-',
            ]);
            DB::table('parameter_penilaian')->insert([
                'Tipe' => "2_0",
                'ParameterID' => "1",
                'NamaParameter' => "Template Parameter",
                'Level' => 1,
                'ChildSum' => 0,
                'Parent' => '0',
            ]);

            //Parameter Penguji
            DB::table('parameter_penilaian')->insert([
                'Tipe' => "2_1",
                'ParameterID' => "-1",
                'NamaParameter' => "Rekapitulasi Nilai Penguji",
                'Level' => -1,
                'ChildSum' => 2,
                'Parent' => '-',
            ]);
            DB::table('parameter_penilaian')->insert([
                'Tipe' => "2_1",
                'ParameterID' => "0",
                'NamaParameter' => "Nilai Penguji",
                'Level' => 0,
                'ChildSum' => 1,
                'Parent' => '-1',
            ]);
            DB::table('parameter_penilaian')->insert([
                'Tipe' => "2_1",
                'ParameterID' => "1",
                'NamaParameter' => "Template Parameter",
                'Level' => 1,
                'ChildSum' => 0,
                'Parent' => '0',
            ]);

        //Sidang
            //Parameter Pembimbing
            DB::table('parameter_penilaian')->insert([
                'Tipe' => "3",
                'ParameterID' => "-",
                'NamaParameter' => "Rekapitulasi Akhir",
                'Level' => -2,
                'ChildSum' => 2,
            ]);
            DB::table('parameter_penilaian')->insert([
                'Tipe' => "3_0",
                'ParameterID' => "0",
                'NamaParameter' => "Nilai Pembimbing",
                'Level' => 0,
                'ChildSum' => 1,
                'Parent' => '-',
            ]);
            DB::table('parameter_penilaian')->insert([
                'Tipe' => "3_0",
                'ParameterID' => "1",
                'NamaParameter' => "Template Parameter",
                'Level' => 1,
                'ChildSum' => 0,
                'Parent' => '0',
            ]);

            //Parameter Penguji
            DB::table('parameter_penilaian')->insert([
                'Tipe' => "3_1",
                'ParameterID' => "-1",
                'NamaParameter' => "Rekapitulasi Nilai Penguji",
                'Level' => -1,
                'ChildSum' => 2,
                'Parent' => '-',
            ]);
            DB::table('parameter_penilaian')->insert([
                'Tipe' => "3_1",
                'ParameterID' => "0",
                'NamaParameter' => "Nilai Penguji",
                'Level' => 0,
                'ChildSum' => 1,
                'Parent' => '-1',
            ]);
            DB::table('parameter_penilaian')->insert([
                'Tipe' => "3_1",
                'ParameterID' => "1",
                'NamaParameter' => "Template Parameter",
                'Level' => 1,
                'ChildSum' => 0,
                'Parent' => '0',
            ]);
    }

    public function data()
    {
        //load the CSV document from a stream
        $csv = Reader::createFromPath(base_path() . '/database/seeds/csvs/SeminarParameterData.csv', 'r');
        $csv->setDelimiter(';');
        $csv->setHeaderOffset(0);

        foreach ($csv as $row) {
            DB::table('parameter_penilaian')->insert([
                'Tipe' => $row["Tipe"],
                'ParameterID' => $row["ParameterID"],
                'NamaParameter' => $row["NamaParameter"],
                'Deskripsi_L' => $row["Deskripsi_L"],
                'Deskripsi_M' => $row["Deskripsi_M"],
                'Deskripsi_K' => $row["Deskripsi_K"],
                'ChildSum' => $row["ChildSum"],
                'Level' => $row["Level"],
                'Parent' => $row["Parent"],
            ]); 
        }
    }
}

class ParameterOptionTableSeeder extends Seeder
{
    public function run()
    {
        $opt = ParameterOption::all();
        if (count($opt) == 0) {
            DB::table('parameter_option')->insert([
                'NilaiParameter' => '0',
                'NamaParameter' => 'B'
            ]);
            DB::table('parameter_option')->insert([
                'NilaiParameter' => '1',
                'NamaParameter' => 'C'
            ]);
            DB::table('parameter_option')->insert([
                'NilaiParameter' => '2',
                'NamaParameter' => 'K'
            ]);
        }
    }
}

class InitialiationSeeder extends Seeder
{

    public function run()
    {
        $dosbing = MahasiswaDosbing::all();
        if (count($dosbing) == 0) {
            $listMahasiswa = Mahasiswa::all()->take(10);
            $listDosen = Dosen::all()->take(10);
            $i = 0;
            foreach ($listMahasiswa as $mahasiswa) {
                MahasiswaDosbing::create([
                    'MahasiswaID' => $mahasiswa->NIM,
                    'DosenID' => $listDosen[$i]->NIP,
                    'Approved' => 1
                ]);
                Mahasiswa::where('NIM', $mahasiswa->NIM)->update(['Approved' => 1, 'Topik' => 'Suka Random ' . strval($i)]);
                $i = $i + 1;
            }
        }
    }
}

class JadwalTableSeeder extends Seeder
{
    public function run()
    {
        for ($i = 1; $i <= 40; $i++) {
            $t = time();
            DB::table('jadwal')->insert([
                'Waktu_Mulai' => date("Y-m-d H:i:s", $t),
                'Waktu_Akhir' => date("Y-m-d H:i:s", $t),
                'Jenis_Kegiatan' => rand(1, 3),
            ]);
        }
    }
}