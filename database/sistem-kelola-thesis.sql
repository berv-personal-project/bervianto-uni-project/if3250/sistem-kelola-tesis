-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 31, 2017 at 12:48 PM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sistem-kelola-thesis`
--

-- --------------------------------------------------------

--
-- Table structure for table `bimbingan`
--

CREATE TABLE `bimbingan` (
  `ID` int(10) UNSIGNED NOT NULL,
  `Tanggal` date NOT NULL,
  `MahasiswaID` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Catatan` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `Rencana` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Triggers `bimbingan`
--
DELIMITER $$
CREATE TRIGGER `bimbingan_trig` AFTER INSERT ON `bimbingan` FOR EACH ROW INSERT INTO bimbingan_dosbing
            SELECT NEW.ID, DosenID, false
            FROM mahasiswa_dosbing
            WHERE mahasiswa_dosbing.MahasiswaID = NEW.MahasiswaID AND mahasiswa_dosbing.Approved = 1
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `bimbingan_dosbing`
--

CREATE TABLE `bimbingan_dosbing` (
  `BimbinganID` int(11) NOT NULL,
  `DosenID` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Approved` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `dosen`
--

CREATE TABLE `dosen` (
  `NIP` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Telepon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `dosen`
--

INSERT INTO `dosen` (`NIP`, `Nama`, `Email`, `Telepon`) VALUES
('195206131979031004', 'Iping Supriana Suwardi', NULL, ''),
('195301161979032001', 'M.M. Inggriani', NULL, ''),
('195305051981031006', 'M. Sukrisno Mardiyanto', NULL, ''),
('195407161980111001', 'Benhard Sitohang', NULL, ''),
('196209121988111001', 'Afwarman', NULL, ''),
('196212031988111001', 'Suhono Harso Supangkat', NULL, ''),
('196312111990011002', 'Suhardi', NULL, ''),
('196408121991021001', 'Kridanto Surendro', NULL, ''),
('196504141991021001', 'Arry Akhmad Arman', NULL, ''),
('196509241995012001', 'Gusti Ayu Putri Saptawati S.', NULL, ''),
('196512101994021001', 'Rinaldi', NULL, ''),
('196602281991021001', 'Jaka Sembiring', NULL, ''),
('196808031993021001', 'Rila Mandala', NULL, ''),
('196812071994021001', 'Dwi Hendratmo Widyantoro', NULL, ''),
('196907291998021001', 'Bayu Hendrajaya', NULL, ''),
('197111291997021001', 'Yusep Rosmansyah', NULL, ''),
('197308092006041001', 'Achmad Imam Kistijantoro', NULL, ''),
('197405091998031002', 'Saiful Akbar', NULL, ''),
('197701102014041001', 'Wikan Danar Sunindyo', NULL, ''),
('197701272008012011', 'Ayu Purwarianti', NULL, ''),
('197912012012122005', 'Dessi Puji Lestari', NULL, '');

-- --------------------------------------------------------

--
-- Table structure for table `jadwal`
--

CREATE TABLE `jadwal` (
  `ID` int(10) UNSIGNED NOT NULL,
  `Waktu_Mulai` date NOT NULL,
  `Waktu_Akhir` date NOT NULL,
  `Jenis_Kegiatan` tinyint(4) NOT NULL,
  `Keterangan` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `jadwal`
--

INSERT INTO `jadwal` (`ID`, `Waktu_Mulai`, `Waktu_Akhir`, `Jenis_Kegiatan`, `Keterangan`) VALUES
(1, '2017-08-31', '2017-08-31', 1, NULL),
(2, '2017-08-31', '2017-08-31', 3, NULL),
(3, '2017-08-31', '2017-08-31', 1, NULL),
(4, '2017-08-31', '2017-08-31', 1, NULL),
(5, '2017-08-31', '2017-08-31', 2, NULL),
(6, '2017-08-31', '2017-08-31', 2, NULL),
(7, '2017-08-31', '2017-08-31', 2, NULL),
(8, '2017-08-31', '2017-08-31', 3, NULL),
(9, '2017-08-31', '2017-08-31', 3, NULL),
(10, '2017-08-31', '2017-08-31', 2, NULL),
(11, '2017-08-31', '2017-08-31', 1, NULL),
(12, '2017-08-31', '2017-08-31', 2, NULL),
(13, '2017-08-31', '2017-08-31', 3, NULL),
(14, '2017-08-31', '2017-08-31', 2, NULL),
(15, '2017-08-31', '2017-08-31', 3, NULL),
(16, '2017-08-31', '2017-08-31', 3, NULL),
(17, '2017-08-31', '2017-08-31', 2, NULL),
(18, '2017-08-31', '2017-08-31', 3, NULL),
(19, '2017-08-31', '2017-08-31', 1, NULL),
(20, '2017-08-31', '2017-08-31', 1, NULL),
(21, '2017-08-31', '2017-08-31', 2, NULL),
(22, '2017-08-31', '2017-08-31', 3, NULL),
(23, '2017-08-31', '2017-08-31', 2, NULL),
(24, '2017-08-31', '2017-08-31', 2, NULL),
(25, '2017-08-31', '2017-08-31', 3, NULL),
(26, '2017-08-31', '2017-08-31', 3, NULL),
(27, '2017-08-31', '2017-08-31', 1, NULL),
(28, '2017-08-31', '2017-08-31', 3, NULL),
(29, '2017-08-31', '2017-08-31', 2, NULL),
(30, '2017-08-31', '2017-08-31', 1, NULL),
(31, '2017-08-31', '2017-08-31', 1, NULL),
(32, '2017-08-31', '2017-08-31', 2, NULL),
(33, '2017-08-31', '2017-08-31', 1, NULL),
(34, '2017-08-31', '2017-08-31', 2, NULL),
(35, '2017-08-31', '2017-08-31', 1, NULL),
(36, '2017-08-31', '2017-08-31', 2, NULL),
(37, '2017-08-31', '2017-08-31', 2, NULL),
(38, '2017-08-31', '2017-08-31', 2, NULL),
(39, '2017-08-31', '2017-08-31', 3, NULL),
(40, '2017-08-31', '2017-08-31', 3, NULL);

--
-- Triggers `jadwal`
--
DELIMITER $$
CREATE TRIGGER `jadwal_trig` AFTER UPDATE ON `jadwal` FOR EACH ROW BEGIN
            UPDATE pengajuan_jadwal
            SET pengajuan_jadwal.Keterangan = NEW.Keterangan,
                pengajuan_jadwal.Jenis_Kegiatan = NEW.Jenis_Kegiatan
            WHERE pengajuan_jadwal.jadwal_id = NEW.ID;
            
            UPDATE seminar_sidang
            SET seminar_sidang.Keterangan = NEW.Keterangan,
                seminar_sidang.Tipe = NEW.Jenis_Kegiatan
            WHERE seminar_sidang.jadwal_id = NEW.ID;
            END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `kategori_seminar_sidang`
--

CREATE TABLE `kategori_seminar_sidang` (
  `ID` int(10) UNSIGNED NOT NULL,
  `Jenis` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Deskripsi` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `kategori_seminar_sidang`
--

INSERT INTO `kategori_seminar_sidang` (`ID`, `Jenis`, `Deskripsi`) VALUES
(1, 'Seminar 1', 'Kegiatan seminar tahap 1'),
(2, 'Seminar 2', 'Kegiatan seminar tahap 2'),
(3, 'Seminar 3', 'Kegiatan seminar tahap 3'),
(4, 'Seminar 4', 'Kegiatan seminar tahap 4'),
(5, 'Seminar 5', 'Kegiatan seminar tahap 5'),
(6, 'Sidang 1', 'Kegiatan sidang tahap 1'),
(7, 'Sidang 2', 'Kegiatan sidang tahap 2'),
(8, 'Sidang 3', 'Kegiatan sidang tahap 3'),
(9, 'Sidang 4', 'Kegiatan sidang tahap 4'),
(10, 'Sidang 5', 'Kegiatan sidang tahap 5');

-- --------------------------------------------------------

--
-- Table structure for table `mahasiswa`
--

CREATE TABLE `mahasiswa` (
  `NIM` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Topik` text COLLATE utf8mb4_unicode_ci,
  `Index` double(8,2) DEFAULT NULL,
  `Approved` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `mahasiswa`
--

INSERT INTO `mahasiswa` (`NIM`, `Nama`, `Email`, `Topik`, `Index`, `Approved`) VALUES
('23514002', 'Adam Asshidiq', NULL, 'Suka Random 0', NULL, 1),
('23514003', 'Malisa Huzaifa', NULL, 'Suka Random 1', NULL, 1),
('23514006', 'Rizki Kurniati', NULL, 'Suka Random 2', NULL, 1),
('23514009', 'Dinda Lestarini', NULL, 'Suka Random 3', NULL, 1),
('23514017', 'Ria Chaniago', NULL, 'Suka Random 4', NULL, 1),
('23514022', 'Eko Mailansa', NULL, 'Suka Random 5', NULL, 1),
('23514023', 'Rita Rahmawati', NULL, 'Suka Random 6', NULL, 1),
('23514029', 'Rahma Djati Kusuma', NULL, 'Suka Random 7', NULL, 1),
('23514030', 'Adi Ferdian Syahputra Panjaita', NULL, 'Suka Random 8', NULL, 1),
('23514045', 'Dwina Satrinia', NULL, 'Suka Random 9', NULL, 1),
('23514062', 'Siti Dwi Setiarini', NULL, '', NULL, 0),
('23514065', 'Setiyo Widayat', NULL, '', NULL, 0),
('23514066', 'Arif Setiawan', NULL, '', NULL, 0),
('23514067', 'Salman Faisal', NULL, '', NULL, 0),
('23514068', 'Oki Fajar Wijayanto', NULL, '', NULL, 0),
('23514070', 'Firyal Fakhrilhadi', NULL, '', NULL, 0),
('23514072', 'Hannif Kurniawan Muttaqin', NULL, '', NULL, 0),
('23514073', 'Donni Richasdy', NULL, '', NULL, 0),
('23514075', 'Rofid Rahmadi', NULL, '', NULL, 0),
('23514078', 'Andre Marga Pradja', NULL, '', NULL, 0),
('23514082', 'Sahru Maskur', NULL, '', NULL, 0),
('23514083', 'Rakhman Imansyah', NULL, '', NULL, 0),
('23514084', 'Anwirman', NULL, '', NULL, 0),
('23514085', 'Muhammad Bahroni', NULL, '', NULL, 0),
('23514089', 'Ridwan Effendi', NULL, '', NULL, 0),
('23514090', 'Donal Siagian', NULL, '', NULL, 0),
('23514094', 'Apip Ramdlani', NULL, '', NULL, 0),
('23514099', 'Mila Ramadiana Harahap', NULL, '', NULL, 0),
('23514102', 'Sendy Ferdian Sujadi', NULL, '', NULL, 0),
('23514105', 'Army Aristofany', NULL, '', NULL, 0),
('23515001', 'Tasya Sukma Maftuhah', NULL, '', NULL, 0),
('23515006', 'Muhammad Ulil Albab', NULL, '', NULL, 0),
('23515012', 'Luh Made Wisnu Satyaninggrat', NULL, '', NULL, 0),
('23515015', 'Zaenal Abidin', NULL, '', NULL, 0),
('23515019', 'Muhammad Indra Nurardy Saputra', NULL, '', NULL, 0),
('23515025', 'Fatan Kasyidi', NULL, '', NULL, 0),
('23515029', 'Dhika Rizki Anbiya', NULL, '', NULL, 0),
('23515030', 'Salman Muhammad Ibadurrahman', NULL, '', NULL, 0),
('23515036', 'Arnold Anugrah Panannangan', NULL, '', NULL, 0),
('23515045', 'Lukman Hakim', NULL, '', NULL, 0),
('23515048', 'Rizki Elisa Nalawati', NULL, '', NULL, 0),
('23515049', 'Ignasius Erwin', NULL, '', NULL, 0),
('23515052', 'Iqsyahiro Kresna A', NULL, '', NULL, 0),
('23515061', 'Beri Noviansyah', NULL, '', NULL, 0),
('23515062', 'YOHANES PERDANA PUTRA', NULL, '', NULL, 0),
('23515068', 'AHMAD FAUZI AULIA', NULL, '', NULL, 0),
('23516081', 'Ivana Clairine Irsan', NULL, '', NULL, 0),
('23516082', 'William Stefan Hartono', NULL, '', NULL, 0),
('23516083', 'Felicia Christie', NULL, '', NULL, 0),
('23516084', 'Khaidzir Muhammad Shahih', NULL, '', NULL, 0),
('23516085', 'Stephen', NULL, '', NULL, 0),
('23516086', 'Mario Tressa Juzar', NULL, '', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `mahasiswa_dosbing`
--

CREATE TABLE `mahasiswa_dosbing` (
  `MahasiswaID` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `DosenID` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Approved` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `mahasiswa_dosbing`
--

INSERT INTO `mahasiswa_dosbing` (`MahasiswaID`, `DosenID`, `Approved`) VALUES
('23514002', '195206131979031004', 1),
('23514003', '195301161979032001', 1),
('23514006', '195305051981031006', 1),
('23514009', '195407161980111001', 1),
('23514017', '196209121988111001', 1),
('23514022', '196212031988111001', 1),
('23514023', '196312111990011002', 1),
('23514029', '196408121991021001', 1),
('23514030', '196504141991021001', 1),
('23514045', '196509241995012001', 1);

-- --------------------------------------------------------

--
-- Table structure for table `nilai`
--

CREATE TABLE `nilai` (
  `NIM` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `SeminarTopik` double(8,2) NOT NULL,
  `Seminar` double(8,2) NOT NULL,
  `Sidang` double(8,2) NOT NULL,
  `NilaiAkhir` double(8,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `parameter_detail`
--

CREATE TABLE `parameter_detail` (
  `Tipe` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ParameterID` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Kriteria` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `HasilParameter` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `parameter_detail`
--

INSERT INTO `parameter_detail` (`Tipe`, `ParameterID`, `Kriteria`, `HasilParameter`) VALUES
('1', '-', 'K-K', '-'),
('1', '-', 'K-M', 'C'),
('1', '-', 'L-L', 'A'),
('1', '-', 'L-M', 'AB'),
('1', '-', 'M-K', 'BC'),
('1', '-', 'M-L', 'AB'),
('1', '-', 'M-M', 'B'),
('1_0', '0', 'L-L', 'L'),
('1_0', '0', 'L-M', 'M'),
('1_0', '0', 'M-K', 'K'),
('1_0', '0', 'M-L', 'M'),
('1_0', '0', 'M-M', 'M'),
('1_0', '1', 'K-M-L', 'K'),
('1_0', '1', 'L-L-L', 'L'),
('1_0', '1', 'L-L-M', 'L'),
('1_0', '1', 'L-M-L', 'L'),
('1_0', '1', 'L-M-M', 'M'),
('1_0', '1', 'M-K-L', 'K'),
('1_0', '1', 'M-K-M', 'K'),
('1_0', '1', 'M-L-K', 'M'),
('1_0', '1', 'M-L-L', 'M'),
('1_0', '1', 'M-L-M', 'M'),
('1_0', '1', 'M-M-M', 'M'),
('1_0', '2', 'K-L-K', 'K'),
('1_0', '2', 'K-L-L', 'K'),
('1_0', '2', 'K-L-M', 'K'),
('1_0', '2', 'K-M-K', 'K'),
('1_0', '2', 'K-M-L', 'K'),
('1_0', '2', 'K-M-M', 'K'),
('1_0', '2', 'L-K-L', 'L'),
('1_0', '2', 'L-L-L', 'L'),
('1_0', '2', 'L-M-L', 'L'),
('1_0', '2', 'M-K-K', 'K'),
('1_0', '2', 'M-L-L', 'M'),
('1_0', '2', 'M-L-M', 'M'),
('1_0', '2', 'M-M-L', 'M'),
('1_0', '2', 'M-M-M', 'M'),
('1_0', '2-1', 'K-K-K', 'K'),
('1_0', '2-1', 'K-K-L', 'K'),
('1_0', '2-1', 'K-K-M', 'K'),
('1_0', '2-1', 'K-L-K', 'K'),
('1_0', '2-1', 'K-L-L', 'K'),
('1_0', '2-1', 'K-L-M', 'K'),
('1_0', '2-1', 'K-M-K', 'K'),
('1_0', '2-1', 'K-M-L', 'K'),
('1_0', '2-1', 'K-M-M', 'K'),
('1_0', '2-1', 'L-L-K', 'L'),
('1_0', '2-1', 'L-L-L', 'L'),
('1_0', '2-1', 'L-L-M', 'L'),
('1_0', '2-1', 'L-M-K', 'L'),
('1_0', '2-1', 'L-M-M', 'L'),
('1_0', '2-1', 'M-L-K', 'M'),
('1_0', '2-1', 'M-L-L', 'M'),
('1_0', '2-1', 'M-L-M', 'M'),
('1_0', '2-1', 'M-M-K', 'M'),
('1_0', '2-1', 'M-M-L', 'M'),
('1_0', '2-1', 'M-M-M', 'M'),
('1_1', '-1', 'K-K', 'K'),
('1_1', '-1', 'L-L', 'L'),
('1_1', '-1', 'M-M', 'M'),
('1_1', '0', 'L-K-L', 'M'),
('1_1', '0', 'L-L-L', 'L'),
('1_1', '0', 'L-L-M', 'L'),
('1_1', '0', 'L-M-L', 'L'),
('1_1', '0', 'M-K-L', 'K'),
('1_1', '0', 'M-K-M', 'K'),
('1_1', '0', 'M-L-M', 'M'),
('1_1', '0', 'M-M-K', 'M'),
('1_1', '0', 'M-M-M', 'M'),
('1_1', '1', 'K-M', 'K'),
('1_1', '1', 'L-K', 'M'),
('1_1', '1', 'L-L', 'L'),
('1_1', '1', 'L-M', 'L'),
('1_1', '1', 'M-K', 'K'),
('1_1', '1', 'M-L', 'L'),
('1_1', '1', 'M-M', 'M'),
('1_1', '2', 'L-L-K', 'L'),
('1_1', '2', 'L-L-L', 'L'),
('1_1', '2', 'L-L-M', 'L'),
('1_1', '2', 'L-M-K', 'L'),
('1_1', '2', 'L-M-M', 'L'),
('1_1', '2', 'M-L-K', 'M'),
('1_1', '2', 'M-L-L', 'M'),
('1_1', '2', 'M-L-M', 'M'),
('1_1', '2', 'M-M-K', 'M'),
('1_1', '2', 'M-M-L', 'M'),
('1_1', '2', 'M-M-M', 'M'),
('1_1', '3', 'L-L-L', 'L'),
('1_1', '3', 'L-L-M', 'L'),
('1_1', '3', 'L-M-L', 'L'),
('1_1', '3', 'M-K-K', 'K'),
('1_1', '3', 'M-L-K', 'M'),
('1_1', '3', 'M-M-M', 'M'),
('1_1', '3-1', 'K-K', 'K'),
('1_1', '3-1', 'K-L', 'K'),
('1_1', '3-1', 'K-M', 'K'),
('1_1', '3-1', 'L-K', 'M'),
('1_1', '3-1', 'L-L', 'L'),
('1_1', '3-1', 'L-M', 'L'),
('1_1', '3-1', 'M-K', 'M'),
('1_1', '3-1', 'M-L', 'M'),
('1_1', '3-1', 'M-M', 'M');

-- --------------------------------------------------------

--
-- Table structure for table `parameter_penilaian`
--

CREATE TABLE `parameter_penilaian` (
  `Tipe` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ParameterID` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `NamaParameter` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Deskripsi_L` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Deskripsi_M` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Deskripsi_K` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ChildSum` int(11) NOT NULL,
  `Level` int(11) NOT NULL,
  `Parent` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `parameter_penilaian`
--

INSERT INTO `parameter_penilaian` (`Tipe`, `ParameterID`, `NamaParameter`, `Deskripsi_L`, `Deskripsi_M`, `Deskripsi_K`, `ChildSum`, `Level`, `Parent`) VALUES
('1', '-', 'Rekapitulasi Akhir', '', '', '', 2, -2, ''),
('1_0', '0', 'Nilai Pembimbing', '', '', '', 2, 0, '-'),
('1_0', '1', 'Kriteria Utama', '', '', '', 3, 1, '0'),
('1_0', '1-1', 'Pendefinisian permasalahan Tesis', '', '', '', 0, 2, '1'),
('1_0', '1-2', 'Pemahaman materi utama Tesis', '', '', '', 0, 2, '1'),
('1_0', '1-3', 'Usaha yang dilakukan untuk mencapai hasil', '', '', '', 0, 2, '1'),
('1_0', '2', 'Kriteria Pendukung', '', '', '', 3, 1, '0'),
('1_0', '2-1', 'Isi laporan', '', '', '', 3, 2, '2'),
('1_0', '2-1-1', 'Kerunutan, kejelasan, dan kelengkapan isi laporan', '', '', '', 0, 3, '2-1'),
('1_0', '2-1-2', 'Ketepatan penggunaan ejaan, tata bahasa, dan tata tulis', '', '', '', 0, 3, '2-1'),
('1_0', '2-1-3', 'Kerapihan dan keterbacaan tulisan dan gambar', '', '', '', 0, 3, '2-1'),
('1_0', '2-2', 'Komunikasi dan tanggungjawab dalam proses bimbingan', '', '', '', 0, 2, '2'),
('1_0', '2-3', 'Kreativitas dalam mencapai hasil', '', '', '', 0, 2, '2'),
('1_1', '-1', 'Rekapitulasi Nilai Penguji', '', '', '', 2, -1, '-'),
('1_1', '0', 'Nilai Penguji', '', '', '', 3, 0, '-1'),
('1_1', '1', 'Kriteria Utama', '', '', '', 2, 1, '0'),
('1_1', '1-1', 'Pendefinisian permasalahan Tesis', '', '', '', 0, 2, '1'),
('1_1', '1-2', 'Pemahaman materi utama Tesis', '', '', '', 0, 2, '1'),
('1_1', '2', 'Kriteria Penting', '', '', '', 3, 1, '0'),
('1_1', '2-1', 'Kerunutan, kejelasan, dan kelengkapan isi laporan', '', '', '', 0, 2, '2'),
('1_1', '2-2', 'Ketepatan penggunaan ejaan, tata bahasa, dan tata tulis', '', '', '', 0, 2, '2'),
('1_1', '2-3', 'Kerapihan dan keterbacaan tulisan dan gambar', '', '', '', 0, 2, '2'),
('1_1', '3', 'Kriteria Pendukung', '', '', '', 3, 1, '0'),
('1_1', '3-1', 'Presentasi', '', '', '', 2, 2, '3'),
('1_1', '3-1-1', 'Kerunutan, kejelasan, kelengkapan, kerapihan dan keterbacaan slides presentasi', '', '', '', 0, 3, '3-1'),
('1_1', '3-1-2', 'Kejelasan ucapan dan ketepatan penggunaan bahasa, Ketepatan waktu, Perhatian ke hadirin', '', '', '', 0, 3, '3-1'),
('1_1', '3-2', 'Kemampuan menjawab pertanyaan dengan lugas dalam sesi Tanya Jawab', '', '', '', 0, 2, '3'),
('1_1', '3-3', 'Pemahaman materi-materi pendukung Tesis', '', '', '', 0, 2, '3');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pengajuan_jadwal`
--

CREATE TABLE `pengajuan_jadwal` (
  `ID` int(10) UNSIGNED NOT NULL,
  `jadwal_id` int(10) UNSIGNED NOT NULL,
  `Tanggal` date NOT NULL,
  `Waktu_Awal` time NOT NULL,
  `Waktu_Akhir` time NOT NULL,
  `Jenis_Kegiatan` tinyint(4) NOT NULL,
  `Keterangan` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MahasiswaID` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Approved` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `seminar_dosen`
--

CREATE TABLE `seminar_dosen` (
  `SeminarID` int(11) NOT NULL,
  `DosenID` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Role` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `seminar_info`
--

CREATE TABLE `seminar_info` (
  `SeminarID` int(10) UNSIGNED NOT NULL,
  `Catatan_1` text COLLATE utf8mb4_unicode_ci,
  `Catatan_2` text COLLATE utf8mb4_unicode_ci,
  `Catatan_3` text COLLATE utf8mb4_unicode_ci,
  `Nilai` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `seminar_nilai`
--

CREATE TABLE `seminar_nilai` (
  `SeminarID` int(11) NOT NULL,
  `Tipe` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `DosenID` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ParameterID` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ParamValue` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `seminar_sidang`
--

CREATE TABLE `seminar_sidang` (
  `ID` int(10) UNSIGNED NOT NULL,
  `jadwal_id` int(10) UNSIGNED NOT NULL,
  `permintaan_jadwal_id` int(10) UNSIGNED NOT NULL,
  `Tanggal` date NOT NULL,
  `Waktu_Awal` time NOT NULL,
  `Waktu_Akhir` time NOT NULL,
  `Ruangan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Keterangan` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MahasiswaID` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `DosenAID` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `DosenBID` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `DosenCID` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Tipe` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Triggers `seminar_sidang`
--
DELIMITER $$
CREATE TRIGGER `edit_seminar` AFTER UPDATE ON `seminar_sidang` FOR EACH ROW UPDATE pengajuan_jadwal
            SET pengajuan_jadwal.Keterangan = NEW.Keterangan,
                pengajuan_jadwal.Jenis_Kegiatan = NEW.Tipe
            WHERE pengajuan_jadwal.ID = NEW.permintaan_jadwal_id
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `sistem-tesis`
--

CREATE TABLE `sistem-tesis` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sistem-tesis`
--

INSERT INTO `sistem-tesis` (`id`, `migration`, `batch`) VALUES
(19, '2014_10_12_000000_create_users_table', 1),
(20, '2014_10_12_100000_create_password_resets_table', 1),
(21, '2017_04_15_235210_create_bimbingan_table', 1),
(22, '2017_04_15_235554_create_bimbingan_dosbing_table', 1),
(23, '2017_04_16_000025_create_dosen_table', 1),
(24, '2017_04_16_000405_create_jadwal_table', 1),
(25, '2017_04_16_000526_create_mahasiswa_table', 1),
(26, '2017_04_16_000628_create_mahasiswa_dosbing_table', 1),
(27, '2017_04_16_000724_create_pengajuan_jadwal_table', 1),
(28, '2017_04_16_001246_create_seminar_dosen_table', 1),
(29, '2017_04_16_012932_create_seminar_sidang_table', 1),
(30, '2017_04_20_052806_create_parameter_penilaian_table', 1),
(31, '2017_04_21_090724_create_nilai_table', 1),
(32, '2017_04_22_095747_create_seminar_nilai_table', 1),
(33, '2017_04_24_050352_create_seminar_info_table', 1),
(34, '2017_04_25_071857_create_trigger', 1),
(35, '2017_05_17_015319_create_parameter_details_table', 1),
(36, '2017_05_22_070752_create_kategori_seminar_sidang', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `username`, `email`, `password`, `role`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Adam Asshidiq', '23514002', NULL, '$2y$10$b0PYRFTkSSW8N95PPu3JbejNGTtQWuBlM.snqBYumQsmEuxHXC6IK', 'Mahasiswa', NULL, '2017-08-31 03:42:55', '2017-08-31 03:42:55'),
(2, 'Malisa Huzaifa', '23514003', NULL, '$2y$10$tHqzMc7znDoRF/X/Fbn./O5LM4Q/Nv76pkTnVVurVGJUC0.hjxp02', 'Mahasiswa', NULL, '2017-08-31 03:42:56', '2017-08-31 03:42:56'),
(3, 'Rizki Kurniati', '23514006', NULL, '$2y$10$4vWQiE..R5qItV.MY1..xOjsFju18q7OdG/QtKmAxjbDty5jt4vqC', 'Mahasiswa', NULL, '2017-08-31 03:42:57', '2017-08-31 03:42:57'),
(4, 'Dinda Lestarini', '23514009', NULL, '$2y$10$5GvjP6PKfHxm9ljtJVKZFuy51g7okIEOzAQYH7/FbPif2WnF1Q9o2', 'Mahasiswa', NULL, '2017-08-31 03:42:57', '2017-08-31 03:42:57'),
(5, 'Ria Chaniago', '23514017', NULL, '$2y$10$X9W0Mz2kqWbRwv78HxLNYuwBJi3oOn61N8zPWYg757IZwz9AZzs4C', 'Mahasiswa', NULL, '2017-08-31 03:42:57', '2017-08-31 03:42:57'),
(6, 'Eko Mailansa', '23514022', NULL, '$2y$10$hDGSWdIQTqvRZRFpHyjbM.SqmI0srH7MQiKDYUBbhLB6amVIUEdUu', 'Mahasiswa', NULL, '2017-08-31 03:42:58', '2017-08-31 03:42:58'),
(7, 'Rita Rahmawati', '23514023', NULL, '$2y$10$5X30qXuadqkj3vL95XA2X.LBy1yLddWVRt8/TuOamTO4hNR.ginja', 'Mahasiswa', NULL, '2017-08-31 03:42:58', '2017-08-31 03:42:58'),
(8, 'Rahma Djati Kusuma', '23514029', NULL, '$2y$10$A554IWQH08HmkIyCh7IzfuJtzhwTcjytwiPaNTZmwYdKbgO3X1kdq', 'Mahasiswa', NULL, '2017-08-31 03:42:58', '2017-08-31 03:42:58'),
(9, 'Adi Ferdian Syahputra Panjaita', '23514030', NULL, '$2y$10$65HtqEc5nLt95uy3QP6IOujmXEjCXE78jlTb/QK786lMYD0U1N1b2', 'Mahasiswa', NULL, '2017-08-31 03:42:59', '2017-08-31 03:42:59'),
(10, 'Dwina Satrinia', '23514045', NULL, '$2y$10$vJhIysZMXBrkbw45.eBIPuNmbfhucpnT.WFdhGcMibhcmrd/x2DOu', 'Mahasiswa', NULL, '2017-08-31 03:42:59', '2017-08-31 03:42:59'),
(11, 'Siti Dwi Setiarini', '23514062', NULL, '$2y$10$xKsfYpoC/dvQowQL4BLGTu186fbZ9Ip.JelKaGC5KHllbhB6SJ/Wq', 'Mahasiswa', NULL, '2017-08-31 03:43:00', '2017-08-31 03:43:00'),
(12, 'Setiyo Widayat', '23514065', NULL, '$2y$10$rho3GjYB3A8/QYozzT6daeWZ3apBUV2XsM3FDPOeJaFayYidmh8LC', 'Mahasiswa', NULL, '2017-08-31 03:43:00', '2017-08-31 03:43:00'),
(13, 'Arif Setiawan', '23514066', NULL, '$2y$10$MbdCFau6ZaAgDb7dv1Q/jeFw1ojaMvRrZPHGzOOuP31tZB3ZtO9Hu', 'Mahasiswa', NULL, '2017-08-31 03:43:00', '2017-08-31 03:43:00'),
(14, 'Salman Faisal', '23514067', NULL, '$2y$10$ZjE7C3Bjw/SILRa.GooFred8CiUYU5FZqsg5/xsFTM6NpxBdhEhye', 'Mahasiswa', NULL, '2017-08-31 03:43:01', '2017-08-31 03:43:01'),
(15, 'Oki Fajar Wijayanto', '23514068', NULL, '$2y$10$rtfuCDcERZaW3bLCMCr5LOrKk0m8fymwIxcUpzzUTfpmv/uVh7LeK', 'Mahasiswa', NULL, '2017-08-31 03:43:01', '2017-08-31 03:43:01'),
(16, 'Firyal Fakhrilhadi', '23514070', NULL, '$2y$10$rJVlm4rC2TKt94kpsJbgeuOGC.Lup9ZU3HzJLucFGhJv7eAVb4/Cm', 'Mahasiswa', NULL, '2017-08-31 03:43:01', '2017-08-31 03:43:01'),
(17, 'Hannif Kurniawan Muttaqin', '23514072', NULL, '$2y$10$MEqyL/FpeZng06WOmceC1OpCmQ4SQyopf1Gk1Ta0yzFXHL79c.Ena', 'Mahasiswa', NULL, '2017-08-31 03:43:02', '2017-08-31 03:43:02'),
(18, 'Donni Richasdy', '23514073', NULL, '$2y$10$0vsBjWAzjVTnzvCNjJzrI./gHrlSc.cK0h79mQp3jtt5UH4Yc3taO', 'Mahasiswa', NULL, '2017-08-31 03:43:02', '2017-08-31 03:43:02'),
(19, 'Rofid Rahmadi', '23514075', NULL, '$2y$10$v/blbjPuBwplu2PYuVmqz.GW9139gbjFpXKoMoPQA4dlkBQotmX92', 'Mahasiswa', NULL, '2017-08-31 03:43:02', '2017-08-31 03:43:02'),
(20, 'Andre Marga Pradja', '23514078', NULL, '$2y$10$GSbGg/0ObCH...a1EVgYFuGjH0BKsV5CuvG1F4w9JFdZcPRpz4yXy', 'Mahasiswa', NULL, '2017-08-31 03:43:03', '2017-08-31 03:43:03'),
(21, 'Sahru Maskur', '23514082', NULL, '$2y$10$VVYY2WW4qPhjnMTzMIMvI.hMKqScSbhsDHpMtoOCgsuPqqmiCK7Sa', 'Mahasiswa', NULL, '2017-08-31 03:43:03', '2017-08-31 03:43:03'),
(22, 'Rakhman Imansyah', '23514083', NULL, '$2y$10$EZFKMn5VAMVCxRkrD9EaxeL96jhSRuIe75R9DFCnr0G17jzpcEq9y', 'Mahasiswa', NULL, '2017-08-31 03:43:03', '2017-08-31 03:43:03'),
(23, 'Anwirman', '23514084', NULL, '$2y$10$fW/m.DIrFRAmRaj64e9noeSYVsKufTXV5HFSd5kxQPjFdxfrJXQQG', 'Mahasiswa', NULL, '2017-08-31 03:43:04', '2017-08-31 03:43:04'),
(24, 'Muhammad Bahroni', '23514085', NULL, '$2y$10$Uey..le5L/8YASY8jJTIUO78rQyWrO5KpkGGNMUxSJe1Ny4qBdnSe', 'Mahasiswa', NULL, '2017-08-31 03:43:04', '2017-08-31 03:43:04'),
(25, 'Ridwan Effendi', '23514089', NULL, '$2y$10$mKoIGlgfnVx0BKNw1JGyBuNQm75JmFIehzXEdugnzzaLRovCM/05S', 'Mahasiswa', NULL, '2017-08-31 03:43:05', '2017-08-31 03:43:05'),
(26, 'Donal Siagian', '23514090', NULL, '$2y$10$PqBtDlukMWPBd3IeZs8UDOetgzpHXaOZem/yTZiW8SX.be0AEdluW', 'Mahasiswa', NULL, '2017-08-31 03:43:05', '2017-08-31 03:43:05'),
(27, 'Apip Ramdlani', '23514094', NULL, '$2y$10$toCReCF57r19DJdWAdjl/OFZv4cJUl8gbAsPlFzGhYGrrpH50Uys.', 'Mahasiswa', NULL, '2017-08-31 03:43:05', '2017-08-31 03:43:05'),
(28, 'Mila Ramadiana Harahap', '23514099', NULL, '$2y$10$vIUn47xQ93/UfAOeYDGkwu5dUd9q/KrTpRhcdyYovlOiCpgclvgQi', 'Mahasiswa', NULL, '2017-08-31 03:43:06', '2017-08-31 03:43:06'),
(29, 'Sendy Ferdian Sujadi', '23514102', NULL, '$2y$10$xD6MQfUycW6wkD83rSxf7uXPbt/yVjFiMFqoqTE6kEiHHz9fVuWle', 'Mahasiswa', NULL, '2017-08-31 03:43:06', '2017-08-31 03:43:06'),
(30, 'Army Aristofany', '23514105', NULL, '$2y$10$9trZ56acJxvXDa76cesPmOGYiCvzXkJgif7H0bkuBFV8jGGb/yFZq', 'Mahasiswa', NULL, '2017-08-31 03:43:06', '2017-08-31 03:43:06'),
(31, 'Tasya Sukma Maftuhah', '23515001', NULL, '$2y$10$mmZtewiM7lUl5x0Od7Cwpe6PE99B/H2Z6ZzYPAAv3Vo4PHG7nAYN6', 'Mahasiswa', NULL, '2017-08-31 03:43:07', '2017-08-31 03:43:07'),
(32, 'Muhammad Ulil Albab', '23515006', NULL, '$2y$10$LKWzy6o1fxjzQXNoyee2/uXv.Ba6irX2fQNn32PfVLmY6/NJDikvK', 'Mahasiswa', NULL, '2017-08-31 03:43:07', '2017-08-31 03:43:07'),
(33, 'Luh Made Wisnu Satyaninggrat', '23515012', NULL, '$2y$10$k31S2wqGUhNLHm3kOshYD.qSuWiyreefZgyVymf5HRs9DRP/.WyCO', 'Mahasiswa', NULL, '2017-08-31 03:43:07', '2017-08-31 03:43:07'),
(34, 'Zaenal Abidin', '23515015', NULL, '$2y$10$DKxZvOBkjchJb6RLte9sLevGKdjJaNIH8X2EXRAieEis2RWGuzNl.', 'Mahasiswa', NULL, '2017-08-31 03:43:08', '2017-08-31 03:43:08'),
(35, 'Muhammad Indra Nurardy Saputra', '23515019', NULL, '$2y$10$c1ypjLvHXeKneUDj4DFnWO.7iBtf0evCqBduF2bfzCpcpLAdNn5Qu', 'Mahasiswa', NULL, '2017-08-31 03:43:08', '2017-08-31 03:43:08'),
(36, 'Fatan Kasyidi', '23515025', NULL, '$2y$10$pt.SxVJO8lLb2zU0rCcaqujT.vwc4FKvFkgwaXf50fu4cbSSBxGDm', 'Mahasiswa', NULL, '2017-08-31 03:43:09', '2017-08-31 03:43:09'),
(37, 'Dhika Rizki Anbiya', '23515029', NULL, '$2y$10$s2eqqKr.qY/ukQMpNYnMD.bGM/c7YMHRQuDFftTdsIKvktI/UqOAu', 'Mahasiswa', NULL, '2017-08-31 03:43:09', '2017-08-31 03:43:09'),
(38, 'Salman Muhammad Ibadurrahman', '23515030', NULL, '$2y$10$9ueFRntW9xqZVsSfQ/3K7O3jKvNhcV5iA/fXcoUFh4aWe9x02rKKG', 'Mahasiswa', NULL, '2017-08-31 03:43:09', '2017-08-31 03:43:09'),
(39, 'Arnold Anugrah Panannangan', '23515036', NULL, '$2y$10$3bePlxD9FNUN8xhhvV5LJ.1rtueRxvqzbiWLIvcAD4iy6pNqhETU.', 'Mahasiswa', NULL, '2017-08-31 03:43:10', '2017-08-31 03:43:10'),
(40, 'Lukman Hakim', '23515045', NULL, '$2y$10$/3hgEMghjORRjUkJiFWqheZPrevCrjbLEgCyVggKnApHyi4ahU6pu', 'Mahasiswa', NULL, '2017-08-31 03:43:10', '2017-08-31 03:43:10'),
(41, 'Rizki Elisa Nalawati', '23515048', NULL, '$2y$10$YpfGJ2sdyNUvf8Je03nis.Zgebo9jszNzmEuHrqX4SYFxwLmFTgTS', 'Mahasiswa', NULL, '2017-08-31 03:43:10', '2017-08-31 03:43:10'),
(42, 'Ignasius Erwin', '23515049', NULL, '$2y$10$gTNe1loYI4YTiX3SM.D3rumQ6W4kfpb62BQF2pgU4aGdnYmPz.ZKO', 'Mahasiswa', NULL, '2017-08-31 03:43:11', '2017-08-31 03:43:11'),
(43, 'Iqsyahiro Kresna A', '23515052', NULL, '$2y$10$typ.TySGjKdb4PXbynIV6OKEXwOJ0Mz7IQvl3WFQwxM.V6gLyoTJe', 'Mahasiswa', NULL, '2017-08-31 03:43:11', '2017-08-31 03:43:11'),
(44, 'Beri Noviansyah', '23515061', NULL, '$2y$10$TsM77XTgRB2ChJD1jfXzTeRHOvDgq0oXcqYYFcS5A1C7n.jF1GSH2', 'Mahasiswa', NULL, '2017-08-31 03:43:11', '2017-08-31 03:43:11'),
(45, 'YOHANES PERDANA PUTRA', '23515062', NULL, '$2y$10$yEeY9hkqPHJrcUuhZN..ne.ua7L/u65A2PnmKi9RCEq3OQ.bZIAoi', 'Mahasiswa', NULL, '2017-08-31 03:43:12', '2017-08-31 03:43:12'),
(46, 'AHMAD FAUZI AULIA', '23515068', NULL, '$2y$10$iJRJdGGQ9bNwiT5VWqql4upD2cnxLU4VGkZ55oxZxL58a2qKAjaEK', 'Mahasiswa', NULL, '2017-08-31 03:43:12', '2017-08-31 03:43:12'),
(47, 'Ivana Clairine Irsan', '23516081', NULL, '$2y$10$LowdhMjdflKhgUf7sTBcIu0/jwOrgeuUo37paG13yL98tP8d7br4O', 'Mahasiswa', NULL, '2017-08-31 03:43:13', '2017-08-31 03:43:13'),
(48, 'William Stefan Hartono', '23516082', NULL, '$2y$10$gDKWdJIxLhzIue5fz86u/.mpInKWPsWHQVKjpxyvwTFRZK7HIKR2m', 'Mahasiswa', NULL, '2017-08-31 03:43:13', '2017-08-31 03:43:13'),
(49, 'Felicia Christie', '23516083', NULL, '$2y$10$hfPnuRry0W6KJXLeYbAYDup6pFlqBlxEszUcyvQe4lI47RE0LlkQm', 'Mahasiswa', NULL, '2017-08-31 03:43:13', '2017-08-31 03:43:13'),
(50, 'Khaidzir Muhammad Shahih', '23516084', NULL, '$2y$10$rISbdooG6ZH.twfmr7RktuO7xdX9pFVqrl3fV7/v4.wvsPCl6I446', 'Mahasiswa', NULL, '2017-08-31 03:43:14', '2017-08-31 03:43:14'),
(51, 'Stephen', '23516085', NULL, '$2y$10$bWxTpuvAbuuW17DF4CIg6OWw/kJ7MSLLC.uWdDzLd6y/jqWmRPSka', 'Mahasiswa', NULL, '2017-08-31 03:43:14', '2017-08-31 03:43:14'),
(52, 'Mario Tressa Juzar', '23516086', NULL, '$2y$10$eG4Ti/uXz51G6IjqfkmukObgYgptvETgEujldGk2iFcZeKs6bp52u', 'Mahasiswa', NULL, '2017-08-31 03:43:14', '2017-08-31 03:43:14'),
(53, 'Benhard Sitohang', '195407161980111001', NULL, '$2y$10$QlH9/FACkalS/SHv1w8Rr.ggCmk9ChCB7iC589pFo5o7O6KL4qOua', 'Dosen', NULL, '2017-08-31 03:43:15', '2017-08-31 03:43:15'),
(54, 'Ayu Purwarianti', '197701272008012011', NULL, '$2y$10$DEVU6e2hR7S2TThEC.Xy1e.2TnvbIsZdoKFqgzkRBa/PIXIop7vIG', 'Dosen', NULL, '2017-08-31 03:43:15', '2017-08-31 03:43:15'),
(55, 'Dessi Puji Lestari', '197912012012122005', NULL, '$2y$10$6vB/nxUvosG8wmXHM4PJH.NiD0ivdzIDXGcgc4SmVf/RPJBq2JPuC', 'Dosen', NULL, '2017-08-31 03:43:15', '2017-08-31 03:43:15'),
(56, 'M. Sukrisno Mardiyanto', '195305051981031006', NULL, '$2y$10$hYD15jKDyZEtH75GCLmYQOJ0S/fdlndJo9s1hGeMC0jNWuDyZmXXa', 'Dosen', NULL, '2017-08-31 03:43:16', '2017-08-31 03:43:16'),
(57, 'Wikan Danar Sunindyo', '197701102014041001', NULL, '$2y$10$uRQ5cn7bGde/FP70OXXWquBsLCi5LOr6/LZouabZRZo77jmF.DJgi', 'Dosen', NULL, '2017-08-31 03:43:16', '2017-08-31 03:43:16'),
(58, 'Iping Supriana Suwardi', '195206131979031004', NULL, '$2y$10$8W3JYpMufunDiXh0f4yKCeJtvfqnui.qrC/74QehMLeFO0eSbfrc2', 'Dosen', 'evGJCJh9ihoz2xHyjkc9iZHWzGVBCPEsrNvEEJ7Jx7Bvylm8z0xogBBP9RMB', '2017-08-31 03:43:17', '2017-08-31 03:43:17'),
(59, 'Saiful Akbar', '197405091998031002', NULL, '$2y$10$5vK3ABpOlz7fXK9FvTQwIerAhYwxlCj6SYHwE0lX24u3RZYbUymIC', 'Dosen', NULL, '2017-08-31 03:43:17', '2017-08-31 03:43:17'),
(60, 'Achmad Imam Kistijantoro', '197308092006041001', NULL, '$2y$10$GranyWoPHGbuX2UcIemKhOWRwcD2FiUkKf6T9OTLCso3hMIV2Ygtq', 'Dosen', NULL, '2017-08-31 03:43:17', '2017-08-31 03:43:17'),
(61, 'Bayu Hendrajaya', '196907291998021001', NULL, '$2y$10$hGiCa6PhhEvaBrA5SrnmCeD0ZqUMeX0Fa3awW1nsG70JLLt/XZKRO', 'Dosen', NULL, '2017-08-31 03:43:18', '2017-08-31 03:43:18'),
(62, 'Jaka Sembiring', '196602281991021001', NULL, '$2y$10$dk.9HVsO8SrEI9Ti9MZEMeW/wgKHgQ/IqBglMTOIYQuZIqpmwtHme', 'Dosen', NULL, '2017-08-31 03:43:18', '2017-08-31 03:43:18'),
(63, 'Afwarman', '196209121988111001', NULL, '$2y$10$tWd183rY1qR353hJkOyql.dQPcsV8tsD3LnaQ8wlgTa/8912q/lJ2', 'Dosen', NULL, '2017-08-31 03:43:18', '2017-08-31 03:43:18'),
(64, 'Yusep Rosmansyah', '197111291997021001', NULL, '$2y$10$rZKVOeMoaPCUONUCWUa2qONhdsErPyEhk4/2OjvQqNea8I1KxH1iu', 'Dosen', NULL, '2017-08-31 03:43:19', '2017-08-31 03:43:19'),
(65, 'Rinaldi', '196512101994021001', NULL, '$2y$10$jT8eIFj2NItUyWKVqcTQOunWq02lyl4K.vptG7SdA6tjxorhP6uvi', 'Dosen', NULL, '2017-08-31 03:43:19', '2017-08-31 03:43:19'),
(66, 'Dwi Hendratmo Widyantoro', '196812071994021001', NULL, '$2y$10$.fuZYV9UsbmmLXF1KhTGaOS8yqNx9gEKaJjUKWHwPidFcrVuyLwnG', 'Dosen', NULL, '2017-08-31 03:43:19', '2017-08-31 03:43:19'),
(67, 'Kridanto Surendro', '196408121991021001', NULL, '$2y$10$M/oLQ.dfDIz/ISf8P/cP/.FqA/fsQP2N/UwxbvoHk0z6oMftlYGwe', 'Dosen', NULL, '2017-08-31 03:43:20', '2017-08-31 03:43:20'),
(68, 'Suhono Harso Supangkat', '196212031988111001', NULL, '$2y$10$xre4B9X.59EEEKdEdAFMyuYwiab.T64yWXPwflSocmfBcx.6g620C', 'Dosen', NULL, '2017-08-31 03:43:20', '2017-08-31 03:43:20'),
(69, 'Arry Akhmad Arman', '196504141991021001', NULL, '$2y$10$OTbqLeJVspGoj85U/zD/2.2VRPsuP4edb1eQM1s6ZlRG.yko1R5wm', 'Dosen', NULL, '2017-08-31 03:43:20', '2017-08-31 03:43:20'),
(70, 'Suhardi', '196312111990011002', NULL, '$2y$10$qx/WKU2T/wtenVX62GUQzu3LzEoya7AaGxcj/KTiTqXokCfxQyXGm', 'Dosen', NULL, '2017-08-31 03:43:21', '2017-08-31 03:43:21'),
(71, 'Gusti Ayu Putri Saptawati S.', '196509241995012001', NULL, '$2y$10$9bdNOqWqNEy0KAG2Ryqj5u1oqCUHfPpOomoPXBqKaEO6SDgJw1QMS', 'Dosen', NULL, '2017-08-31 03:43:21', '2017-08-31 03:43:21'),
(72, 'Rila Mandala', '196808031993021001', NULL, '$2y$10$hKw6WcX0wKczfqIy9OtTR.hzWpa4btnb4OGXb7JCadnqu8tYMbvVW', 'Dosen', NULL, '2017-08-31 03:43:22', '2017-08-31 03:43:22'),
(73, 'M.M. Inggriani', '195301161979032001', NULL, '$2y$10$TKaxmXoezxCuhIduGS9GVu8/Zmvwrq5MrjQcT68H5VlQFhH9Wbhye', 'Dosen', NULL, '2017-08-31 03:43:22', '2017-08-31 03:43:22'),
(74, 'Administrator', 'admin', NULL, '$2y$10$NTSnC18MAR.JXGxk4NjZT.cBqatUrt5hw46ZzlS1ujYk.jrL5T2sy', 'Admin', NULL, '2017-08-31 03:43:22', '2017-08-31 03:43:22'),
(75, 'Bayu Hendrajaya', 'kaprodi', NULL, '$2y$10$VhshKlCtxLav2mTEaJh/k.YxwE0rmHnbmhHlj9ZkPVV57faD.qHRm', 'Kaprodi', NULL, '2017-08-31 03:43:22', '2017-08-31 03:43:22');

--
-- Triggers `users`
--
DELIMITER $$
CREATE TRIGGER `user_email_trig` AFTER UPDATE ON `users` FOR EACH ROW IF (NEW.role = 'Mahasiswa') THEN
                BEGIN
                    UPDATE mahasiswa
                    SET mahasiswa.Email = NEW.email
                    WHERE mahasiswa.NIM = NEW.id;
                END;
            ELSEIF (NEW.role = 'Dosen') THEN
                BEGIN
                    UPDATE dosen
                    SET dosen.Email = NEW.email
                    WHERE dosen.NIP = NEW.id;
                END;
            END IF
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `user_trig` AFTER INSERT ON `users` FOR EACH ROW IF (NEW.role = 'Mahasiswa') THEN BEGIN INSERT INTO mahasiswa (NIM, Nama, Email, Topik, Approved)
            VALUES (NEW.username, NEW.name, NEW.email, '', false); END;
            ELSEIF (NEW.role = 'Dosen') THEN BEGIN INSERT INTO dosen (NIP, Nama, Email, Telepon)
            VALUES (NEW.username, NEW.name, NEW.email, ''); END; END IF
$$
DELIMITER ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bimbingan`
--
ALTER TABLE `bimbingan`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `bimbingan_mahasiswaid_index` (`MahasiswaID`);

--
-- Indexes for table `bimbingan_dosbing`
--
ALTER TABLE `bimbingan_dosbing`
  ADD PRIMARY KEY (`BimbinganID`,`DosenID`);

--
-- Indexes for table `dosen`
--
ALTER TABLE `dosen`
  ADD PRIMARY KEY (`NIP`);

--
-- Indexes for table `jadwal`
--
ALTER TABLE `jadwal`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `kategori_seminar_sidang`
--
ALTER TABLE `kategori_seminar_sidang`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `mahasiswa_dosbing`
--
ALTER TABLE `mahasiswa_dosbing`
  ADD PRIMARY KEY (`MahasiswaID`,`DosenID`);

--
-- Indexes for table `nilai`
--
ALTER TABLE `nilai`
  ADD PRIMARY KEY (`NIM`);

--
-- Indexes for table `parameter_detail`
--
ALTER TABLE `parameter_detail`
  ADD PRIMARY KEY (`Tipe`,`ParameterID`,`Kriteria`);

--
-- Indexes for table `parameter_penilaian`
--
ALTER TABLE `parameter_penilaian`
  ADD PRIMARY KEY (`Tipe`,`ParameterID`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `pengajuan_jadwal`
--
ALTER TABLE `pengajuan_jadwal`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `pengajuan_jadwal_jadwal_id_foreign` (`jadwal_id`),
  ADD KEY `pengajuan_jadwal_mahasiswaid_index` (`MahasiswaID`);

--
-- Indexes for table `seminar_dosen`
--
ALTER TABLE `seminar_dosen`
  ADD PRIMARY KEY (`SeminarID`,`DosenID`);

--
-- Indexes for table `seminar_info`
--
ALTER TABLE `seminar_info`
  ADD PRIMARY KEY (`SeminarID`);

--
-- Indexes for table `seminar_nilai`
--
ALTER TABLE `seminar_nilai`
  ADD PRIMARY KEY (`SeminarID`,`Tipe`,`DosenID`,`ParameterID`);

--
-- Indexes for table `seminar_sidang`
--
ALTER TABLE `seminar_sidang`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `seminar_sidang_jadwal_id_foreign` (`jadwal_id`),
  ADD KEY `seminar_sidang_permintaan_jadwal_id_foreign` (`permintaan_jadwal_id`),
  ADD KEY `seminar_sidang_mahasiswaid_index` (`MahasiswaID`);

--
-- Indexes for table `sistem-tesis`
--
ALTER TABLE `sistem-tesis`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_username_unique` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bimbingan`
--
ALTER TABLE `bimbingan`
  MODIFY `ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `jadwal`
--
ALTER TABLE `jadwal`
  MODIFY `ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT for table `kategori_seminar_sidang`
--
ALTER TABLE `kategori_seminar_sidang`
  MODIFY `ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `pengajuan_jadwal`
--
ALTER TABLE `pengajuan_jadwal`
  MODIFY `ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `seminar_info`
--
ALTER TABLE `seminar_info`
  MODIFY `SeminarID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `seminar_sidang`
--
ALTER TABLE `seminar_sidang`
  MODIFY `ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sistem-tesis`
--
ALTER TABLE `sistem-tesis`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=76;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `pengajuan_jadwal`
--
ALTER TABLE `pengajuan_jadwal`
  ADD CONSTRAINT `pengajuan_jadwal_jadwal_id_foreign` FOREIGN KEY (`jadwal_id`) REFERENCES `jadwal` (`ID`) ON DELETE CASCADE;

--
-- Constraints for table `seminar_sidang`
--
ALTER TABLE `seminar_sidang`
  ADD CONSTRAINT `seminar_sidang_jadwal_id_foreign` FOREIGN KEY (`jadwal_id`) REFERENCES `jadwal` (`ID`) ON DELETE CASCADE,
  ADD CONSTRAINT `seminar_sidang_permintaan_jadwal_id_foreign` FOREIGN KEY (`permintaan_jadwal_id`) REFERENCES `pengajuan_jadwal` (`ID`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
