<?php
/**
 * Created by PhpStorm.
 * User: BerviantoLeoPratama
 * Date: 15/08/2017
 * Time: 7:53
 */

namespace App\Http\Controllers;


class HelpController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index() {
        return view('help.qna');
    }
}