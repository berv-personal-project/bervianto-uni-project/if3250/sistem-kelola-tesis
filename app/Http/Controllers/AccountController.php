<?php
/**
 * Created by PhpStorm.
 * User: BerviantoLeoPratama
 * Date: 10/08/2017
 * Time: 10:13
 */

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use League\Csv\Reader;

class AccountController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index() {
        $role = strtolower(Auth::user()->role);
        if ($role == 'admin') {
            return view('account.index');
        } else {
            return redirect()->action('HomeController@index');
        }
    }

    public function addMahasiswa(Request $request) {
        $role = strtolower(Auth::user()->role);
        if ($role == 'admin') {
            if ($request->hasFile('mahasiswa')) {
                $file = $request->file('mahasiswa');
                if ($file->isValid()) {
                    $path = $file->getRealPath();
                    $stream = fopen($path, 'r');
                    $csv = Reader::createFromStream($stream)
                        ->setDelimiter(';')
                        ->setOffset(1);

                    $record = $csv->fetchAll();
                    $errorList = array();
                    foreach ($record as $row) {
                        $user = User::where('username', $row[1])->first();
                        if (empty($user)) {
                            $t = time();
                            DB::table('users')->insert([
                                'name' => $row[2],
                                'username' => $row[1],
                                'password' => bcrypt('test123'),
                                'role' => "Mahasiswa",
                                'created_at' => date("Y-m-d H:i:s", $t),
                                'updated_at' => date("Y-m-d H:i:s", $t),
                            ]);
                        } else {
                            array_push($errorList, $user);
                        }
                    }
                    if (empty($errorList)) {
                        return redirect()->action('AccountController@index')->with("success", "Your file uploaded");
                    } else {
                        return redirect()->action('AccountController@index')->with("error", $errorList);
                    }
                }
            } else {
                return redirect()->action('AccountController@index')->with("errorText", "File not uploaded");
            }
        } else {
            return redirect()->action('HomeController@index');
        }
    }

    public function addDosen(Request $request) {
        $role = strtolower(Auth::user()->role);
        if ($role == 'admin') {
            if ($request->hasFile('dosen')) {
                $file = $request->file('dosen');
                if ($file->isValid()) {
                    $path = $file->getRealPath();
                    $stream = fopen($path, 'r');
                    $csv = Reader::createFromStream($stream)
                        ->setDelimiter(';')
                        ->setOffset(1);

                    $record = $csv->fetchAll();
                    $errorList = array();
                    foreach ($record as $row) {
                        $user = User::where('username', $row[1])->first();
                        if (empty($user)) {
                            $t = time();
                            DB::table('users')->insert([
                                'name' => $row[2],
                                'username' => $row[1],
                                'password' => bcrypt('test123'),
                                'role' => "Dosen",
                                'created_at' => date("Y-m-d H:i:s", $t),
                                'updated_at' => date("Y-m-d H:i:s", $t),
                            ]);
                        } else {
                            array_push($errorList, $user);
                        }
                    }
                    if (empty($errorList)) {
                        return redirect()->action('AccountController@index')->with("success", "Your file uploaded");
                    } else {
                        return redirect()->action('AccountController@index')->with("error", $errorList);
                    }
                }
            } else {
                return redirect()->action('AccountController@index')->with("errorText", "File not uploaded");
            }
        } else {
            return redirect()->action('HomeController@index');
        }
    }


}