<?php
/**
 * Created by PhpStorm.
 * User: BerviantoLeoPratama
 * Date: 08/08/2017
 * Time: 18:58
 */

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function showProfile()
    {
        $user = Auth::user();
        return view('profile.showprofile')
            ->with('user', $user);
    }

    public function editProfile()
    {
        $user = Auth::user();
        return view('profile.editprofile')
            ->with('user', $user);
    }

    public function updateProfile(Request $request)
    {
        $email = $request->email;
        $password = $request->password;
        $confirm_password = $request->confirm_password;
        $user = Auth::user();
        if (!empty($password) && !empty($confirm_password)) {
            if ($password == $confirm_password) {
                if (strlen($password) > 4) {
                    $user->password = $password;
                    $user->save();
                } else {
                    return redirect('edit-profile');
                }
            }
        }
        if (!empty($email)) {
            $user->email = $email;
            $user->save();
        }
        return redirect('show-profile');
    }


}