<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SeminarSidang extends Model
{
    protected $table = 'seminar_sidang';
    
    public $timestamps = false;

    protected $fillable = [
    	 'ID', 'jadwal_id', 'permintaan_jadwal_id', 'Tanggal', 'Waktu_Awal', 'Waktu_Akhir', 'Ruangan', 'Keterangan', 'MahasiswaID', 'DosenAID', 'DosenBID', 'DosenCID', 'Tipe',
    ];
}
