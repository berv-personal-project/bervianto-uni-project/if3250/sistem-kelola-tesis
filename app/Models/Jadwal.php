<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Jadwal extends Model
{
    protected $table = 'jadwal';

    public $timestamps = false;

    protected $fillable = [
        'ID', 'Waktu_Mulai', 'Waktu_Akhir', 'Jenis_Kegiatan', 'Keterangan',
    ];
}
