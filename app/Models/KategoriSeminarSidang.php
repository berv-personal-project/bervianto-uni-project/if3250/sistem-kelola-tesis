<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KategoriSeminarSidang extends Model
{
    protected $table = 'kategori_seminar_sidang';

    public $primaryKey = 'ID';

    public $timestamps = false;

    public $incrementing = true;

    protected $fillable = [
        'ID', 'Jenis', 'Deskripsi',
    ];
}
