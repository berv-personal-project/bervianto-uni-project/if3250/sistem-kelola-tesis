<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SeminarNilai extends Model
{
    protected $table = 'seminar_nilai';

    public $primaryKey = 'SeminarID';

    public $timestamps = false;

    protected $fillable = [
        'SeminarID', 'Tipe', 'DosenID', 'ParameterID', 'ParamValue',
    ];


    public static function find($p1, $p2, $p3) {
	    return Widget::where('SeminarID', $p1)
	        ->where('DosenID', $p2)
	        ->where('ParameterID', $p3)
	        ->first();
    }
}
