<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BimbinganDosbing extends Model
{
    protected $table = 'bimbingan_dosbing';

    public $primaryKey = 'BimbinganID';

    public $timestamps = false;
}
