<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ParameterOption extends Model
{
    protected $table = 'parameter_option';

    public $primaryKey = 'NilaiParameter';

    public $timestamps = false;

	public $incrementing = false;

}
