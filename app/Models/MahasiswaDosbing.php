<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MahasiswaDosbing extends Model
{
    protected $table = 'mahasiswa_dosbing';

    public $primaryKey = 'MahasiswaID';

    public $timestamps = false;

    protected $fillable = [
        'MahasiswaID', 'DosenID', 'Approved',
    ];

}
