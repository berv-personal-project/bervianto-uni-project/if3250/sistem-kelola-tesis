@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Admin Dashboard</div>

                    <div class="panel-body">
                        <p>Nama Pengguna : {{Auth::user()->name}}</p>
                        <p style="color:red; font-weight: bold;">Telah masuk sebagai {{Auth::user()->role}}.</p>
                        <p style="color:green; font-weight: bold;">Lihat Menu untuk Fasilitas yang dapat digunakan.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
