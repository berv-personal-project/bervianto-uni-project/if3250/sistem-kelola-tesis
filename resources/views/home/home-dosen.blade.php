@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                @if(empty(Auth::user()->email))
                    <div class="alert alert-warning">
                        Jika ingin memanfaatkan fitur reset password. Tambahkan email melalui <a
                                href="{{url('edit-profile')}}">edit profile</a>.
                    </div>
                @endif
                <div class="panel panel-default">
                    <div class="panel-heading">Dashboard Dosen</div>

                    <div class="panel-body">
                        <p>Nama : <b style="color:green;">{{ Auth::user()->name }}</b></p>
                        <p>NIP : <b style="color:green;">{{ Auth::user()->username }}</b></p>

                        <?php $i = 0 ?>
                        <p>Daftar Mahasiswa Bimbingan:</p>
                        @if($mahasiswa->isEmpty())
                            <p><b style="color:red;">Belum ada Mahasiswa Bimbingan</b></p>
                        @else
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th>NIM</th>
                                        <th>Nama</th>
                                        <th>Topik</th>
                                        <th>Persetujuan</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($mahasiswa as $mhs)
                                        <tr>
                                            <td>
                                                <a href="mahasiswa/{{ $mhs->MahasiswaID }}"> {{ $mhs->MahasiswaID }}</a>
                                            </td>
                                            <td>
                                                {{ $namaMahasiswa[$i]->Nama }}
                                            </td>
                                            @if ($mhs->Approved == 0)
                                                <td class="text-warning">Belum mengajukan topik</td>
                                                <td>
                                                    <form action="/approve-mahasiswa" method="POST">
                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="MahasiswaID"
                                                               value="{{ $mhs->MahasiswaID }}">
                                                        <input type="hidden" name="DosenID"
                                                               value="{{ $dosenDetail->NIP }}">
                                                        <input type="submit" name="option" class="btn btn-primary"
                                                               style="margin-right: 6px" value="Approve">
                                                        <input type="submit" name="option" class="btn btn-danger"
                                                               value="Decline">
                                                    </form>
                                                </td>
                                            @else
                                                @if (($namaMahasiswa[$i]->Approved != 1) AND ($namaMahasiswa[$i]->Topik != ''))
                                                    <td>
                                                        <span class="text-info">{{ $namaMahasiswa[$i]->Topik }}</span>
                                                    </td>
                                                    <td>
                                                        <form action="/approve-topik" method="POST">
                                                            {{ csrf_field() }}
                                                            <input type="hidden" name="MahasiswaID"
                                                                   value="{{ $mhs->MahasiswaID }}">
                                                            <input type="submit" name="option" class="btn btn-primary"
                                                                   style="margin-right: 6px"
                                                                   value="Approve">
                                                            <input type="submit" name="option" class="btn btn-danger"
                                                                   value="Decline">
                                                        </form>
                                                    </td>
                                                @elseif ($namaMahasiswa[$i]->Approved == 1)
                                                    <td>
                                                        <div class="text-info">{{ $namaMahasiswa[$i]->Topik }}</div>
                                                    </td>
                                                    <td><b class="text-success">Sudah Diterima, Topik Sudah
                                                            Disetujui</b></td>
                                                @else
                                                    <td class="text-warning">Belum mengajukan topik</td>
                                                    <td><b class="text-warning">Sudah Diterima, Topik Belum
                                                            Disetujui</b></td>
                                                @endif
                                            @endif

                                            <?php $i++ ?>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
