@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Request Dosen Pembimbing</div>

                    <div class="panel-body">
                        {{ $mahasiswaDetail->NIM }} <br/>
                        {{ $mahasiswaDetail->Nama }} <br/>
                        {{ $mahasiswaDetail->Email }} <br/>
                        {{ $mahasiswaDetail->Topik }} <br/>
                        {{ $mahasiswaDetail->Index }} <br/>
                        {{ $mahasiswaDetail->Approved }} <br/>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
