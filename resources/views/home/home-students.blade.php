@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                @if(empty(Auth::user()->email))
                    <div class="alert alert-warning">
                        Jika ingin memanfaatkan fitur reset password. Tambahkan email melalui <a
                                href="{{url('edit-profile')}}">edit profile</a>.
                    </div>
                @endif
                <div class="panel panel-default">
                    <div class="panel-heading">Profil Mahasiswa</div>

                    <div class="panel-body">
                        @if( ! empty($errorText))
                            <div class="alert alert-danger">
                                <strong>Error!</strong> {{ $errorText }}
                            </div>
                        @endif
                        <br/>
                        <p>Nama : <b style="color:green;">{{ $mahasiswaDetail->Nama }}</b></p>
                        <p> NIM : <b style="color:green;">{{ $mahasiswaDetail->NIM }}</b></p>
                        @if ($mahasiswaDetail->Approved == 1)
                            <p> Topik : <b style="color:green;">{{ $mahasiswaDetail->Topik }}</b> <span
                                        style="color:green;">(telah disetujui)</span></p>
                        @endif

                        @if (!$dosbing->isEmpty())
                            <br/>
                            <p><strong> List Dosen Pembimbing : </strong></p>
                            <?php $i = 1; ?>
                            <ul>
                                @foreach($dosbing as $dosen)
                                    <li><span> Dosen pembimbing {{ $i }} : {{ $dosen->Nama }} </span>
                                        <?php $i++ ?>
                                        @if ($dosen->Approved == -1)
                                            <form style="display: inline-block; margin-left:30px;"
                                                  class="form-horizontal" role="form"
                                                  method="POST" action="/deleteReqDos">
                                                {{ csrf_field() }}
                                                <input type="hidden" name="DosenID" value="{{ $dosen->DosenID }}">
                                                <div style="margin-bottom: 0px;" class="form-group">
                                                    <div class="col-md-6 col-md-offset-4">
                                                        <button type="submit" class="btn btn-danger"
                                                                style="float: right; padding: 2px; font-size: 11px;">
                                                            Hapus
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                            <div class="text-danger"> ({{ $dosen->msg }})</div>
                                        @elseif ($dosen->Approved == 1)
                                            <div class="text-success"> ({{ $dosen->msg }})</div>
                                        @elseif ($dosen->Approved == 0)
                                            <div class="text-warning"> ({{ $dosen->msg }})</div>
                                        @endif
                                    </li>
                                    <br>
                                @endforeach
                            </ul>
                            @if ($mahasiswaDetail->Approved != 1)
                                <hr/>
                                <p>
                                    <strong class="text-warning">Setelah mengisi topik, dosen pembimbing tidak dapat
                                        diubah!</strong>
                                </p>
                                <p>
                                    <strong>Ajukan topik :</strong>
                                </p>
                                <form class="form-horizontal" role="form" method="POST" action="/editTopik">
                                    {{ csrf_field() }}

                                    <div class="form-group">
                                        <label for="NIPDosen" style="color:black;"
                                               class="col-md-2 control-label">Topik</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" name="Topik" required autofocus/>
                                        </div>
                                        <button type="submit" class="btn btn-primary">
                                            Ajukan
                                        </button>
                                    </div>
                                </form>
                                <br/>
                            @endif
                        @else
                            <p class="text-danger"><strong>Belum ada dosen pembimbing</strong></p>
                        @endif

                        @if (($mahasiswaDetail->Topik != '') AND ($mahasiswaDetail->Approved == 0))
                            <strong class="text-warning">Topik : </strong> {{ $mahasiswaDetail->Topik }}<br/>
                            <strong class="text-info">(belum disetujui)</strong>
                        @elseif ($mahasiswaDetail->Approved != 1)
                            <p>
                                @if ($mahasiswaDetail->Approved == 0)
                                    <strong class="text-danger">Belum menambahkan topik</strong>
                                @else
                                    <strong class="text-danger">Topik ditolak</strong>
                                @endif
                            </p>
                            @if ($mahasiswaDetail->Approved == 0)
                                <hr/>
                                <p>
                                    <strong>Tambahkan dosen pembimbing :</strong>
                                </p>
                                <form class="form-horizontal" role="form" method="POST" action="/addDosenPembimbing">
                                    {{ csrf_field() }}


                                    <div class="form-group">
                                        <label for="NIPDosen" class="col-md-3 control-label">NIP Dosen
                                            Pembimbing</label>
                                        <div class="col-md-6">
                                            <select name="NIPDosen" class="form-control">
                                                <!--  TODO Automatic Update after Selection Mahasiswa -->
                                                <option disabled selected>Pilih Dosen</option>
                                                @foreach ($listDosen as $entity)
                                                    <option value="{{$entity->ID}}">{{$entity->ID}}
                                                        | {{$entity->Nama}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <button type="submit" class="btn btn-primary">
                                            Tambahkan
                                        </button>
                                    </div>
                                </form>
                            @endif
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
