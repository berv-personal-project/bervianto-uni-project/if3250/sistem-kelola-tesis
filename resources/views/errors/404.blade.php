@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">404 | Not Found</div>
                    <div class="panel-body">
                        Not Found.
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
