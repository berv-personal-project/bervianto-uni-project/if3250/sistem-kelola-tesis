<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('css/app.css') }}" type="text/css"/>
    <link rel="stylesheet" href="{{ asset('css/extended.css') }}" type="text/css"/>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
          rel="stylesheet">
    <link rel="icon" href="{{asset('favicon.png')}}" type="image/png"/>

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>

<body>
<div id="app">
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <a href="{{ url('/') }}">
                    <img class="navbar-brand" src="{{asset('images/logo-if-abet-computing.png')}}"
                         href="{{url('/')}}">
                    <!-- Branding Image -->
                </a>
                <a style="color:#fff;" class="navbar-brand" href="{{ url('/') }}">
                    <b>{{ config('app.name', 'Laravel') }}</b>
                </a>

            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @if (Auth::guest())
                        <li><a href="{{ route('login') }}">Masuk</a></li>
                    @else
                        @if (Auth::user()->role === 'Dosen')
                            <li>
                                <a href="{{ url('mahasiswa') }}">
                                    Mahasiswa Bimbingan
                                </a>
                            </li>
                        @endif
                        <li class="dropdown">
                            <a id="tesis-menu" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-expanded="false">
                                <b>Menu</b> <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu" role="menu">
                                <li>
                                    <a href="{{ url('bimbingan') }}">
                                        Bimbingan
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ url('jadwal') }}">
                                        Jadwal
                                    </a>
                                </li>
                                @if (Auth::user()->role === 'Admin')
                                    <li>
                                        <a href="{{ url('listseminar') }}">
                                            Daftar Seminar
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ url('daftarparameter') }}">
                                            Daftar Parameter
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ url('mahasiswa') }}">
                                            Daftar Mahasiswa
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ url('dosen') }}">
                                            Daftar Dosen
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{url('add-account')}}">Manajemen Akun</a>
                                    </li>
                                @elseif (Auth::user()->role === 'Dosen')
                                    <li>
                                        <a href="{{ url('seminar') }}">
                                            Seminar
                                        </a>
                                    </li>
                                @elseif (Auth::user()->role === 'Kaprodi')
                                    <li>
                                        <a href="{{ url('listseminar') }}">
                                            Daftar Seminar
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ url('mahasiswa') }}">
                                            Daftar Mahasiswa
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ url('dosen') }}">
                                            Daftar Dosen
                                        </a>
                                    </li>
                                @endif

                            </ul>
                        </li>

                        <!-- Global -->
                        <li class="dropdown">
                            <a id="user-menu" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-expanded="false">
                                <b>{{ Auth::user()->username }} | {{ Auth::user()->name }} </b> <span
                                        class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                @if (!Auth::guest())
                                    <li><a href="/show-profile">Profile</a></li>
                                    <li><a href="/help">Bantuan</a></li>
                                @endif

                                <li>
                                    <a href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        Keluar
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                          style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>

    @yield('content')
</div>

<!-- Scripts -->
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/ui-jp.config.js') }}"></script>
<script src="{{ asset('js/ui-jp.js') }}"></script>
<script src="{{ asset('js/ui-load.js') }}"></script>
<script type="text/javascript">
    $("#addSession").submit(function (event) {
        var x = confirm("Apakah Anda yakin ingin menambahkan Sesi?");
        if (x) {
            return true;
        }
        else {
            event.preventDefault();
            return false;
        }
    });
</script>

</body>

<footer id="main-footer">
    <div class="container" style="margin-bottom: 30px;">
        <p class="text-muted credit"><span style="text-align: left; float: left">&copy; 2017 <a href="#">Program Studi Magister Teknik Informatika</a></span>
            <span class="hidden-phone"
                  style="text-align: right; float: right">Powered by: <a
                        href="http://laravel.com/" alt="Laravel 8">Laravel 8</a></span></p>
    </div>
</footer>
@yield('page-js-files')
@yield('page-js-script')
</html>
