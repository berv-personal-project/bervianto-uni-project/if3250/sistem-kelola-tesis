@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Daftar Parameter</div>
                    <div class="panel-body">
                        <div class="center">
                            <ul class="nav nav-pills">
                                <li class="active"><a data-toggle="pill" href="#proposal">
                                        Seminar Proposal
                                    </a></li>
                                <li><a data-toggle="pill" href="#seminar">
                                        Seminar
                                    </a></li>
                                <li><a data-toggle="pill" href="#sidang">
                                        Sidang
                                    </a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="tab-content">
                            <div id="proposal" class="tab-pane fade in active">
                                @if (!empty($param1))
                                    <div class="field_wrapper">
                                        <div id="proposal_pembimbing">
                                            <a href="{{ url('editparameter', ['tipe' => '1', 'id' => $param1->ParameterID]) }}">
                                                {!! $param1->NamaParameter !!}
                                            </a>
                                            <br>
                                            <br>
                                            <label>Parameter Pembimbing</label>
                                            <br>
                                            @foreach ($param1a as $i => $out)
                                                <?php $num = explode("_", $out->ParameterID); ?>
                                                <a href="{{ url('editparameter', ['tipe' => '1_0', 'id' => $out->ParameterID]) }}">
                                                    @foreach ($num as $n)
                                                        {!! $n !!}.
                                                    @endforeach
                                                    {!! $out->NamaParameter !!}
                                                </a>
                                                <br>
                                            @endforeach

                                        </div>
                                        <br>
                                        <div id="proposal_penguji">
                                            <label>Parameter Penguji</label>
                                            <br>
                                            @foreach ($param1b as $i => $out)
                                                <?php $num = explode("_", $out->ParameterID); ?>
                                                <a href="{{ url('editparameter', ['tipe' => '1_1', 'id' => $out->ParameterID]) }}">
                                                    @foreach ($num as $n)
                                                        {!! $n !!}.
                                                    @endforeach
                                                    {!! $out->NamaParameter !!}
                                                </a>
                                                <br>
                                            @endforeach
                                        </div>
                                    </div>
                                @endif
                            </div>
                            <div id="seminar" class="tab-pane fade">
                                @if(!empty($param2))
                                    <div class="field_wrapper">
                                        <div id="seminar_pembimbing">
                                            <a href="{{ url('editparameter', ['tipe' => '2', 'id' => $param2->ParameterID]) }}">
                                                {!! $param2->NamaParameter !!}
                                            </a>
                                            <br>
                                            <br>
                                            <label>Parameter Pembimbing</label>
                                            <br>
                                            @foreach ($param2a as $i => $out)
                                                <?php $num = explode("_", $out->ParameterID); ?>
                                                <a href="{{ url('editparameter', ['tipe' => '2_0', 'id' => $out->ParameterID]) }}">
                                                    @foreach ($num as $n)
                                                        {!! $n !!}.
                                                    @endforeach
                                                    {!! $out->NamaParameter !!}
                                                </a>
                                                <br>
                                            @endforeach
                                        </div>
                                        <br>
                                        <div id="seminar_penguji">
                                            <label>Parameter Penguji</label>
                                            <br>
                                            @foreach ($param2b as $i => $out)
                                                <?php $num = explode("_", $out->ParameterID); ?>
                                                <a href="{{ url('editparameter', ['tipe' => '2_1', 'id' => $out->ParameterID]) }}">
                                                    @foreach ($num as $n)
                                                        {!! $n !!}.
                                                    @endforeach
                                                    {!! $out->NamaParameter !!}
                                                </a>
                                                <br>
                                            @endforeach
                                        </div>
                                    </div>
                                @endif
                            </div>
                            <div id="sidang" class="tab-pane fade">
                                @if(!empty($param3))
                                    <div class="field_wrapper">
                                        <div id="sidang_pembimbing">
                                            <a href="{{ url('editparameter', ['tipe' => '3', 'id' => $param3->ParameterID]) }}">
                                                {!! $param3->NamaParameter !!}
                                            </a>
                                            <br>
                                            <br>
                                            <label>Parameter Pembimbing</label>
                                            <br>
                                            @foreach ($param3a as $i => $out)
                                                <?php $num = explode("_", $out->ParameterID); ?>
                                                <a href="{{ url('editparameter', ['tipe' => '3_0', 'id' => $out->ParameterID]) }}">
                                                    @foreach ($num as $n)
                                                        {!! $n !!}.
                                                    @endforeach
                                                    {!! $out->NamaParameter !!}
                                                </a>
                                                <br>
                                            @endforeach
                                        </div>
                                        <br>
                                        <div id="sidang_penguji">
                                            <label>Parameter Penguji</label>
                                            <br>
                                            @foreach ($param3b as $i => $out)
                                                <?php $num = explode("_", $out->ParameterID); ?>
                                                <a href="{{ url('editparameter', ['tipe' => '3_1', 'id' => $out->ParameterID]) }}">
                                                    @foreach ($num as $n)
                                                        {!! $n !!}.
                                                    @endforeach
                                                    {!! $out->NamaParameter !!}
                                                </a>
                                                <br>
                                            @endforeach
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
