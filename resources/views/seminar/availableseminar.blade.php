@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Daftar Seminar</div>

                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th class="text-center">NIM Mahasiswa</th>
                                    <th class="text-center">Topik</th>
                                    <th class="text-center">Penilaian</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($list as $i => $info)
                                    <tr>
                                        <td class="text-center">{!! $info->MahasiswaID !!}</td>
                                        <td class="text-center">{!! $topik[$i] !!}</td>
                                        <td class="text-center">
                                            <a href="{{ url('seminarsession', ['id' => $info->SeminarID]) }}">Klik
                                                Disini</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection