@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                @if( ! empty($errorText))
                    <div class="alert alert-danger">
                        <strong>Error!</strong> {{ $errorText }}
                    </div>
                @endif
                <div class="panel panel-default">
                    <div class="panel-heading">Persetujuan Jadwal
                        <div style="float: right;">
                            <a href="/viewPengajuan">
                                <button type="button" class="btn btn-primary btn-sm">Kembali</button>
                            </a>
                        </div>
                    </div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="/jadwal/postApproval">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="nim" class="col-md-4 control-label">NIM Mahasiswa</label>
                                <div class="col-md-6">
                                    <input type="text" id="nim" class="form-control" name="nim"
                                           value="<?php echo $pj->MahasiswaID; ?>" disabled/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="nama" class="col-md-4 control-label">Nama Mahasiswa</label>
                                <div class="col-md-6">
                                    <input type="text" id="nama" class="form-control" name="nama"
                                           value="<?php echo $pj->Nama; ?>" disabled/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="tanggal" class="col-md-4 control-label">Tanggal</label>
                                <div class="col-md-6">
                                    <input id="tanggal" type="text" class="form-control" name="tanggal"
                                           value="<?php echo $pj->Tanggal; ?>" disabled/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="waktu_aw" class="col-md-4 control-label">Waktu Awal</label>
                                <div class="col-md-6">
                                    <input id="waktu_aw" type="text" class="form-control" name="waktu_aw"
                                           value="<?php echo $pj->Waktu_Awal; ?>" disabled/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="waktu_a" class="col-md-4 control-label">Waktu Akhir</label>
                                <div class="col-md-6">
                                    <input id="waktu_a" type="text" class="form-control" name="waktu_a"
                                           value="<?php echo $pj->Waktu_Akhir; ?>" disabled/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="jenis">Jenis Kegiatan</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" id="jenis" name="jenis"
                                           value="<?php echo $pj->Jenis_Kegiatan; ?>" disabled/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="keterangan" class="col-md-4 control-label">Keterangan</label>
                                <div class="col-md-6">
                                    <input type="text" id="keterangan" class="form-control" name="keterangan"
                                           value="<?php echo $pj->Keterangan; ?>"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="ruangan" class="col-md-4 control-label">Ruangan</label>
                                <div class="col-md-6">
                                    <input id="ruangan" type="text" class="form-control" name="ruangan"
                                           value="<?php echo $pj->Ruangan; ?>" required autofocus/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="dosen1" class="col-md-4 control-label">NIP Dosen Pembimbing</label>
                                <div class="col-md-6">
                                    <input id="dosen1" class="form-control" name="dosen1"
                                           value="<?php echo $pj->dosenA; ?>" disabled/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="dosen2" class="col-md-4 control-label">NIP Dosen Penguji 1</label>
                                <div class="col-md-6">
                                    <input id="dosen2" class="form-control" name="dosen2"
                                           value="<?php echo $pj->dosenB; ?>" disabled/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="dosen3" class="col-md-4 control-label">NIP Dosen Penguji 2</label>
                                <div class="col-md-6">
                                    <input id="dosen3" class="form-control" name="dosen3"
                                           value="<?php echo $pj->dosenC; ?>" disabled/>
                                </div>
                            </div>
                            <div class="form-group">
                                <input type="hidden" name="pjID" value="{{ $pj->ID }}">
                                <input type="hidden" name="tanggal" value="{{ $pj->Tanggal }}">
                                <input type="hidden" name="waktu_aw" value="{{ $pj->Waktu_Awal }}">
                                <input type="hidden" name="waktu_a" value="{{ $pj->Waktu_Akhir }}">
                                <input type="hidden" name="nim" value="{{ $pj->MahasiswaID }}">
                                <input type="hidden" name="jenis" value="{{ $pj->Jenis }}">
                                <input type="hidden" name="dosen1" value="{{ $pj->dosenA }}">
                                <input type="hidden" name="dosen2" value="{{ $pj->dosenB }}">
                                <input type="hidden" name="dosen3" value="{{ $pj->dosenC }}">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Konfirmasi
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection