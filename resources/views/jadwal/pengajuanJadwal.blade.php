@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                @if( ! empty($errorText))
                    <div class="alert alert-danger">
                        <strong>Insert failed!</strong> {{ $errorText }}
                    </div>
                @endif
                <div class="panel panel-default">
                    <div class="panel-heading">Jadwal Baru
                        <div style="float: right;">
                            <a href="/jadwal">
                                <button type="button" class="btn btn-primary btn-sm">Kembali</button>
                            </a>
                        </div>
                    </div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="/jadwal">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="tanggal" class="col-md-4 control-label">Tanggal</label>
                                <div class="col-md-6">
                                    <input type="hidden" name="tgl_awal" value="{{ $jadwal->Waktu_Mulai }}">
                                    <input type="hidden" name="tgl_akhir" value="{{ $jadwal->Waktu_Akhir }}">
                                    <input id="tanggal" type="text" class="form-control" name="tanggal"
                                           value="<?php echo date('d-m-Y'); ?>" required autofocus/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="waktu_aw" class="col-md-4 control-label">Waktu Awal</label>
                                <div class="col-md-6">
                                    <input id="waktu_aw" type="text" class="form-control" name="waktu_aw"
                                           value="<?php echo date('h:i:s'); ?>" required autofocus/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="waktu_a" class="col-md-4 control-label">Waktu Akhir (minimal 90
                                    menit)</label>
                                <div class="col-md-6">
                                    <input id="waktu_a" type="text" class="form-control" name="waktu_a"
                                           value="<?php echo date('h:i:s'); ?>" required autofocus/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="jenis">Jenis Kegiatan</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" id="jenis" name="jenis"
                                           value="<?php echo $jadwal->Jenis_Kegiatan; ?>" disabled>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="keterangan" class="col-md-4 control-label">Keterangan</label>
                                <div class="col-md-6">
                                    <input type="text" id="keterangan" class="form-control" name="keterangan"
                                           value="<?php echo $jadwal->Keterangan; ?>" disabled/>
                                </div>
                            </div>
                            <div class="form-group">
                                <input type="hidden" name="idJadwal" value="{{ $jadwal->ID }}"/>
                                <input type="hidden" name="jenis" value="{{ $jadwal->Jenis }}"/>
                                <input type="hidden" name="keterangan" value="{{ $jadwal->Keterangan }}"/>
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Ajukan
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection