@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                @if( ! empty($errorText))
                    <div class="alert alert-danger">
                        <strong>Insert failed!</strong> {{ $errorText }}
                    </div>
                @endif
                <div class="panel panel-default">
                    <div class="panel-heading">Pengajuan Jadwal
                        <div style="float: right;">
                            <a href="/jadwal">
                                <button type="button" class="btn btn-primary btn-sm">Kembali</button>
                            </a>
                        </div>
                    </div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="" action="">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="nim" class="col-md-4 control-label">NIM Mahasiswa</label>
                                <div class="col-md-6">
                                    <input type="text" id="nim" class="form-control" name="nim"
                                           value="<?php echo $pj->MahasiswaID; ?>" disabled/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="nama" class="col-md-4 control-label">Nama Mahasiswa</label>
                                <div class="col-md-6">
                                    <input type="text" id="nama" class="form-control" name="nama"
                                           value="<?php echo $pj->Nama; ?>" disabled/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="tanggal" class="col-md-4 control-label">Tanggal</label>
                                <div class="col-md-6">
                                    <input id="tanggal" type="text" class="form-control" name="tanggal"
                                           value="<?php echo $pj->Tanggal; ?>" disabled/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="waktu_aw" class="col-md-4 control-label">Waktu Awal</label>
                                <div class="col-md-6">
                                    <input id="waktu_aw" type="text" class="form-control" name="waktu_aw"
                                           value="<?php echo $pj->Waktu_Awal; ?>" disabled/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="waktu_a" class="col-md-4 control-label">Waktu Akhir</label>
                                <div class="col-md-6">
                                    <input id="waktu_a" type="text" class="form-control" name="waktu_a"
                                           value="<?php echo $pj->Waktu_Akhir; ?>" disabled/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="jenis">Jenis Kegiatan</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" id="jenis" name="jenis"
                                           value="<?php echo $pj->Jenis_Kegiatan; ?>" disabled/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="keterangan" class="col-md-4 control-label">Keterangan</label>
                                <div class="col-md-6">
                                    <input type="text" id="keterangan" class="form-control" name="keterangan"
                                           value="<?php echo $pj->Keterangan; ?>" disabled/>
                                </div>
                            </div>
                        </form>
                        <div class="col-md-6 col-md-offset-4" style="margin-bottom: 30px; margin-left: 29.5%;">
                            <form style="display:inline-block;" action="/jadwal/approve" method="GET">
                                {{ csrf_field() }}
                                <input type="hidden" name="pjID" value="{{ $pj->ID }}">
                                <input type="submit" class="btn btn-primary" value="Approve">
                            </form>
                            <form style="display:inline-block;" action="/jadwal/decline" method="POST">
                                {{ csrf_field() }}
                                <input type="hidden" name="pjID" value="{{ $pj->ID }}">
                                <input type="submit" class="btn btn-danger" value="Decline">
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection