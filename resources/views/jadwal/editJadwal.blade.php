@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                @if( ! empty($errorText))
                    <div class="alert alert-danger">
                        <strong>Insert failed!</strong> {{ $errorText }}
                    </div>
                @endif
                <div class="panel panel-default">
                    <div class="panel-heading">Jadwal Baru
                        <div style="float: right;">
                            <a href="/jadwal/delete/{{ $jadwal->ID }}">
                                <button type="button" class="btn btn-danger btn-sm">Hapus</button>
                            </a>
                            <a href="/jadwal">
                                <button type="button" class="btn btn-primary btn-sm">Kembali</button>
                            </a>
                        </div>
                    </div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="PUT"
                              action="/jadwal/edited/{{ $jadwal->ID }}">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="waktu_m" class="col-md-4 control-label">Waktu Mulai</label>
                                <div class="col-md-6">
                                    <input id="waktu_m" type="text" class="form-control" name="waktu_m"
                                           value="<?php echo $jadwal->Waktu_Mulai; ?>" required autofocus/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="waktu_a" class="col-md-4 control-label">Waktu Akhir</label>
                                <div class="col-md-6">
                                    <input id="waktu_a" type="text" class="form-control" name="waktu_a"
                                           value="<?php echo $jadwal->Waktu_Akhir; ?>" required autofocus/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="jenis">Jenis Kegiatan</label>
                                <div class="col-md-6">
                                    <select name="jenis" class="form-control"
                                            value="<?php echo $jadwal->Jenis_Kegiatan; ?>">
                                        <!--  TODO Automatic Update after Selection Mahasiswa -->
                                        @foreach ($lj as $entity)
                                            <option value="{{$entity->Jenis}}">{{$entity->Jenis}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="kegiatan" class="col-md-4 control-label">Keterangan</label>
                                <div class="col-md-6">
                                    <input id="kegiatan" class="form-control" value="<?php echo $jadwal->Keterangan; ?>"
                                           name="keterangan" autofocus/>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Ubah
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection