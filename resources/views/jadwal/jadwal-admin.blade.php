@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Dashboard</div>
                    <br><br>
                    <ul class="nav nav-pills">
                        <li class="active"><a data-toggle="pill" href="#jadwal">Jadwal</a></li>
                        <li><a data-toggle="pill" href="#jadwalMhs">Jadwal Mahasiswa</a></li>
                    </ul>
                    <br><br>

                    <div class="tab-content">
                        <div id="jadwal" class="tab-pane fade in active">
                            <div>
                                <form class="form-inline" action="/jadwal/jadwalSpec" method="POST">
                                    {{ csrf_field() }}
                                    <div class="col-md-4 text-center">
                                        <div class="form-group">
                                            <label for="bulan">Bulan</label>
                                            <select class="form-control" id="bulan" name="bulan">
                                                <option value="1">Januari</option>
                                                <option value="2">Februari</option>
                                                <option value="3">Maret</option>
                                                <option value="4">April</option>
                                                <option value="5">Mei</option>
                                                <option value="6">Juni</option>
                                                <option value="7">Juli</option>
                                                <option value="8">Agustus</option>
                                                <option value="9">September</option>
                                                <option value="10">Oktober</option>
                                                <option value="11">November</option>
                                                <option value="12">Desember</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4 text-center">
                                        <div class="form-group">
                                            <label for="tahun">Tahun</label>
                                            <select class="form-control" id="tahun" name="tahun">
                                                <option value="2017">2017</option>
                                                <option value="2018">2018</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4 text-center">
                                        <Input type="submit" class="btn btn-primary btn-block" value="Cari">
                                    </div>
                                </form>
                            </div>
                            <br> <br> <br>

                            <div>
                                <table class="table table-hover table-responsive">
                                    <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Tanggal</th>
                                        <th>Waktu</th>
                                        <th>Kegiatan</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    @if ( !$jadwals->isEmpty() )
                                        <tbody>
                                        @foreach ($jadwals as $key => $j)
                                            <tr>
                                                <td>{{ $key+1 }}</td>
                                                <td>{{ $j->Tanggal }}</td>
                                                <td>{{ $j->Waktu_Mulai }} - {{ $j->Waktu_Akhir }}</td>
                                                <td>{{ $j->Kegiatan }}</td>
                                                <td><a href="jadwal/edit/{{ $j->ID }}">
                                                        <button class="btn btn-default btn-block">Edit</button>
                                                    </a></td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    @else
                                        Belum ada jadwal
                                    @endif
                                </table>
                            </div>
                            <br><br>
                            <a href="/jadwal/jadwalBaru">
                                <button class="btn btn-primary btn-block">Add Jadwal</button>
                            </a>
                        </div>

                        <div id="jadwalMhs" class="tab-pane fade">
                            <div>
                                <form class="form-inline" action="/jadwal/jadwalSpec" method="POST">
                                    {{ csrf_field() }}
                                    <div class="col-md-4 text-center">
                                        <div class="form-group">
                                            <label for="bulan">Bulan</label>
                                            <select class="form-control" id="bulan" name="bulan">
                                                <option value="1">Januari</option>
                                                <option value="2">Februari</option>
                                                <option value="3">Maret</option>
                                                <option value="4">April</option>
                                                <option value="5">Mei</option>
                                                <option value="6">Juni</option>
                                                <option value="7">Juli</option>
                                                <option value="8">Agustus</option>
                                                <option value="9">September</option>
                                                <option value="10">Oktober</option>
                                                <option value="11">November</option>
                                                <option value="12">Desember</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4 text-center">
                                        <div class="form-group">
                                            <label for="tahun">Tahun</label>
                                            <select class="form-control" id="tahun" name="tahun">
                                                <option value="2017">2017</option>
                                                <option value="2018">2018</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4 text-center">
                                        <Input type="submit" class="btn btn-primary btn-block" value="Cari">
                                    </div>
                                </form>
                            </div>
                            <br> <br> <br>

                            <div>
                                <table class="table table-hover table-responsive">
                                    <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Mahasiswa</th>
                                        <th>Tanggal</th>
                                        <th>Waktu</th>
                                        <th>Kegiatan</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    @if ( !$pjadwals->isEmpty() )
                                        <tbody>
                                        @foreach ($pjadwals as $key => $pj)
                                            <tr>
                                                <td>{{ $key + 1 }}</td>
                                                <td>{{ $pj->Nama }}</td>
                                                <td>{{ $pj->Tanggal }}</td>
                                                <td>{{ $pj->Waktu }}</td>
                                                <td>{{ $pj->Kegiatan }}</td>
                                                @if ($pj->Approved == '0')
                                                    <td>
                                                        <form action="/jadwal/approve" method="POST">
                                                            {{ csrf_field() }}
                                                            <input type="hidden" name="pjID" value="{{ $pj->ID }}">
                                                            <input type="submit" class="btn btn-primary"
                                                                   value="Approve">
                                                        </form>
                                                    </td>
                                                @else
                                                    <td>
                                                        <button class="btn btn-default btn-block" disabled="true">
                                                            Approved
                                                        </button>
                                                    </td>
                                                @endif
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    @else
                                        Belum ada jadwal
                                    @endif
                                </table>
                            </div>
                            <br><br>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection