@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                @if( ! empty($errorText))
                    <div class="alert alert-danger">
                        <strong>Error!</strong> {{ $errorText }}
                    </div>
                @endif
                <div class="panel panel-default">
                    <div class="panel-heading">Jadwal Baru
                        <div style="float: right;">
                            <a href="/jadwal">
                                <button type="button" class="btn btn-primary btn-sm">Kembali</button>
                            </a>
                        </div>
                    </div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="/jadwal">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="waktu_m" class="col-md-4 control-label">Waktu Mulai</label>
                                <div class="col-md-6">
                                    <input id="waktu_m" type="date" class="form-control" name="waktu_m"
                                           value="<?php echo date('Y-m-d'); ?>" min="<?php echo date('Y-m-d'); ?>" required autofocus/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="waktu_a" class="col-md-4 control-label">Waktu Akhir</label>
                                <div class="col-md-6">
                                    <input id="waktu_a" type="date" class="form-control" name="waktu_a"
                                           value="<?php echo date('Y-m-d'); ?>" min="<?php echo date('Y-m-d'); ?>" required autofocus/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="jenis">Jenis Kegiatan</label>
                                <div class="col-md-6">
                                    <select name="jenis" class="form-control">
                                        <!--  TODO Automatic Update after Selection Mahasiswa -->
                                        <option disabled selected>Pilih Jenis Kegiatan</option>
                                        @foreach ($lj as $entity)
                                            <option value="{{$entity->ID}}">{{$entity->Jenis}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="keterangan" class="col-md-4 control-label">Keterangan</label>
                                <div class="col-md-6">
                                    <input id="keterangan" class="form-control" name="keterangan" autofocus/>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Tambahkan
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection