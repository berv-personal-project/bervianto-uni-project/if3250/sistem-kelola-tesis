@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Bimbingan</div>
                    <div class="panel-body">

                        <ul class="nav nav-tabs">
                            <?php
                            $idxLi = 0;
                            $idxAp = 0;
                            ?>
                            @foreach($mahasiswas as $mahasiswa)
                                @if ($idxLi == 0)
                                    <li class="active"><a data-toggle="tab"
                                                          href="#{{ $mahasiswa->MahasiswaID }}">{{ $mahasiswa->Nama }}</a>
                                    </li>
                                @else
                                    <li><a data-toggle="tab"
                                           href="#{{ $mahasiswa->MahasiswaID }}">{{ $mahasiswa->Nama }}</a></li>
                                @endif
                                <?php $idxLi = $idxLi + 1; ?>
                            @endforeach
                        </ul>

                        <div class="tab-content">
                            <?php $idxLi = 0;?>
                            @foreach($mahasiswas as $mahasiswa)
                                @if ($idxLi == 0)

                                    <div id="{{ $mahasiswa->MahasiswaID }}" class="tab-pane fade in active">
                                        <h3>{{ $mahasiswa->Nama }} - {{ $mahasiswa->MahasiswaID }}</h3>
                                        @if( !$bimbingans[$idxLi]->isEmpty() )
                                            <div class="table-responsive">
                                                <table ui-jq="dataTable" class="table table-hover">
                                                    <thead>
                                                    <tr>
                                                        <th>Tanggal</th>
                                                        <th>Catatan</th>
                                                        <th>Rencana</th>
                                                        <th>Persetujuan</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach ($bimbingans[$idxLi] as $bimbingan)
                                                        <tr>
                                                            <td><a href="bimbingan/{{ $bimbingan->ID }}">
                                                                    {{ date("d-m-Y", strtotime($bimbingan->Tanggal)) }}
                                                                </a></td>
                                                            <td>
                                                                <div class="limit-text-small">
                                                                    {{ $bimbingan->Catatan }}<br/>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="limit-text-small">
                                                                    {{ $bimbingan->Rencana }}<br/>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                @if ($bimbingan->Approved == 0)
                                                                    <form style="margin-bottom: 0px;"
                                                                          action="/approve-bimbingan" method="POST">
                                                                        {{ csrf_field() }}
                                                                        <input type="hidden" name="BimbinganID"
                                                                               value="{{ $bimbingan->ID }}">
                                                                        <input type="hidden" name="DosenID"
                                                                               value="{{ Auth::user()->username }}">
                                                                        <input type="submit" class="btn btn-default"
                                                                               value="Approve">
                                                                    </form>
                                                                @else
                                                                    Sudah disetujui
                                                                @endif
                                                                <?php $idxAp = $idxAp + 1; ?>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        @else
                                            Belum ada bimbingan yang dilakukan
                                        @endif
                                    </div>
                                @else
                                    <div id="{{ $mahasiswa->MahasiswaID }}" class="tab-pane fade">
                                        <h3>{{ $mahasiswa->Nama }} - {{ $mahasiswa->MahasiswaID }}</h3>
                                        @if( !$bimbingans[$idxLi]->isEmpty() )
                                            <table class="table table-hover table-responsive">
                                                <thead>
                                                <tr>
                                                    <th>Tanggal</th>
                                                    <th>Catatan</th>
                                                    <th>Rencana</th>
                                                    <th>Approve</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach ($bimbingans[$idxLi] as $bimbingan)
                                                    <tr>
                                                        <td><a href="bimbingan/{{ $bimbingan->ID }}">
                                                                {{ date("d-m-Y", strtotime($bimbingan->Tanggal)) }}
                                                            </a></td>
                                                        <td>
                                                            <div class="limit-text-small">
                                                                {{ $bimbingan->Catatan }}<br/>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="limit-text-small">
                                                                {{ $bimbingan->Rencana }}<br/>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            @if ($bimbingan->Approved == 0)
                                                                <form action="/approve-bimbingan" method="POST">
                                                                    {{ csrf_field() }}
                                                                    <input type="hidden" name="BimbinganID"
                                                                           value="{{ $bimbingan->ID }}">
                                                                    <input type="hidden" name="DosenID"
                                                                           value="{{ Auth::user()->username }}">
                                                                    <input type="submit" class="btn btn-default"
                                                                           value="Approve">
                                                                </form>
                                                            @else
                                                                Sudah disetujui
                                                            @endif
                                                            <br/>
                                                            <?php $idxAp = $idxAp + 1; ?>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        @else
                                            Belum ada bimbingan yang dilakukan
                                        @endif
                                    </div>
                                @endif
                                <?php $idxLi = $idxLi + 1;?>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
