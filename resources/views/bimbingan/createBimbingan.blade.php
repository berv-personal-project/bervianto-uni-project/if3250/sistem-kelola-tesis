@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                @if( ! empty($errorText))
                    <div class="alert alert-danger">
                        <strong>Insert failed!</strong> {{ $errorText }}
                    </div>
                @endif

                <div class="panel panel-default">
                    <div class="panel-heading">Bimbingan
                        <div style="float: right;">
                            <a href="/bimbingan">
                                <button type="button" class="btn btn-primary btn-sm">Kembali</button>
                            </a>
                        </div>
                    </div>

                    <div class="panel-body">


                        <form class="form-horizontal" role="form" method="POST" action="/bimbingan">
                            {{ csrf_field() }}

                            <div class="form-group">
                                <label for="tanggal" class="col-md-4 control-label">Tanggal</label>
                                <div class="col-md-6">
                                    <input id="tanggal" type="text" class="form-control" name="tanggal"
                                           value="<?php echo date('d-m-Y'); ?>" placeholder="dd-mm-yyyy" required
                                           autofocus/>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="isi" class="col-md-4 control-label">Catatan Bimbingan</label>
                                <div class="col-md-6">
                                    <textarea id="isi" class="form-control fixed-size" rows="10" name="isi" required
                                              autofocus></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="isi" class="col-md-4 control-label">Rencana Selanjutnya</label>
                                <div class="col-md-6">
                                    <textarea id="isi" class="form-control fixed-size" rows="5" name="rencana" required
                                              autofocus></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Catat
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
