@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Bimbingan</div>
                    <div class="panel-body">

                        <ul class="nav nav-tabs">
                            <?php
                            $idxLi = 0;
                            $idxAp = 0;
                            ?>
                            @foreach($mahasiswas as $mahasiswa)
                                @if ($idxLi == 0)
                                    <li class="active"><a data-toggle="tab"
                                                          href="#{{ $mahasiswa->MahasiswaID }}">{{ $mahasiswa->Nama }}</a>
                                    </li>
                                @else
                                    <li><a data-toggle="tab"
                                           href="#{{ $mahasiswa->MahasiswaID }}">{{ $mahasiswa->Nama }}</a></li>
                                @endif
                                <?php $idxLi = $idxLi + 1; ?>
                            @endforeach
                        </ul>

                        <div class="tab-content">
                            <?php $idxLi = 0;?>
                            @foreach($mahasiswas as $mahasiswa)
                                @if ($idxLi == 0)

                                    <div id="{{ $mahasiswa->MahasiswaID }}" class="tab-pane fade in active">
                                        <h3>{{ $mahasiswa->Nama }} - {{ $mahasiswa->MahasiswaID }}</h3>
                                        @if( !$bimbingans[$idxLi]->isEmpty() )
                                            @foreach ($bimbingans[$idxLi] as $bimbingan)
                                                @if (!$approveds[$bimbingan->ID]->isEmpty())
                                                    <div class="list-group">
                                                        <a href="bimbingan/{{ $bimbingan->ID }}"
                                                           class="list-group-item">
                                                            {{ date("d-m-Y", strtotime($bimbingan->Tanggal)) }}<br/>
                                                            <div class="limit-text">
                                                                {{ $bimbingan->Catatan }}<br/>
                                                            </div>
                                                            @if ($approveds[$bimbingan->ID][0]->Approved == 0)
                                                                Belum disetujui
                                                                <form action="/approve-bimbingan" method="POST">
                                                                    {{ csrf_field() }}
                                                                    <input type="hidden" name="BimbinganID"
                                                                           value="{{ $bimbingan->ID }}">
                                                                    <input type="hidden" name="DosenID"
                                                                           value="{{ $dosenID }}">
                                                                    <input type="submit" class="btn btn-default"
                                                                           style="float: right;" value="Approve">
                                                                </form>
                                                            @else
                                                                Sudah disetujui
                                                            @endif
                                                            <br/>
                                                            <?php $idxAp = $idxAp + 1; ?>
                                                        </a>
                                                    </div>
                                                @endif
                                            @endforeach
                                        @else
                                            Belum ada bimbingan yang dilakukan
                                        @endif
                                    </div>
                                @else
                                    <div id="{{ $mahasiswa->MahasiswaID }}" class="tab-pane fade">
                                        <h3>{{ $mahasiswa->Nama }} - {{ $mahasiswa->MahasiswaID }}</h3>
                                        @if( !$bimbingans[$idxLi]->isEmpty() )
                                            @foreach ($bimbingans[$idxLi] as $bimbingan)
                                                @if (!$approveds[$bimbingan->ID]->isEmpty())
                                                    <div class="list-group">
                                                        <a href="bimbingan/{{ $bimbingan->ID }}"
                                                           class="list-group-item">
                                                            {{ date("d-m-Y", strtotime($bimbingan->Tanggal)) }}<br/>
                                                            <div class="limit-text">
                                                                {{ $bimbingan->Catatan }}<br/>
                                                            </div>
                                                            @if ($approveds[$bimbingan->ID][0]->Approved == 0)
                                                                Belum disetujui
                                                                <form style="margin-bottom: 0px;"
                                                                      action="/approve-bimbingan" method="POST">
                                                                    {{ csrf_field() }}
                                                                    <input type="hidden" name="BimbinganID"
                                                                           value="{{ $bimbingan->ID }}">
                                                                    <input type="hidden" name="DosenID"
                                                                           value="{{ $dosenID }}">
                                                                    <input type="submit" class="btn btn-default"
                                                                           style="float: right;" value="Approve">
                                                                </form>
                                                            @else
                                                                Sudah disetujui
                                                            @endif
                                                            <?php $idxAp = $idxAp + 1; ?>
                                                        </a>
                                                    </div>
                                                @endif
                                            @endforeach
                                        @else
                                            Belum ada bimbingan yang dilakukan
                                        @endif
                                    </div>
                                @endif
                                <?php $idxLi = $idxLi + 1;?>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
