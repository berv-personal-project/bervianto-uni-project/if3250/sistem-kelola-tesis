@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Detil Bimbingan</div>
                    <div class="panel-body">
                        <b>Tanggal</b> : {{ date("d-m-Y", strtotime($bimbingan->Tanggal)) }}<br/>
                        <b>Mahasiswa</b> : {{ $bimbingan->MahasiswaID }} (NIM) <br/><br/>
                        <b>Catatan</b> : <br/>
                        <div class="wrap-text">
                            {{ $bimbingan->Catatan }}<br/>
                        </div>
                        <br/><br/>
                        <b>Rencana Selanjutnya</b> : <br/>
                        <div class="wrap-text">
                            {{ $bimbingan->Rencana }}<br/>
                        </div>
                        <br/>
                        <b>Disetujui oleh</b> : <br/>
                        <?php $approvedCount = 0 ?>
                        @foreach($bimbingan->Approved as $key => $aprv)
                            @if ($aprv == 1)
                                {{ $bimbingan->DosenID[$key] }} - {{ $bimbingan->Dosen[$key] }}
                                <br>
                                <?php $approvedCount++ ?>
                            @endif
                        @endforeach

                        @if($approvedCount == 0)
                            Belum ada yang menyetujui
                        @endif

                        @foreach($bimbingan->Approved as $key => $aprv)
                            @if (($aprv == 0) AND (Auth::user()->username == $bimbingan->DosenID[$key]))
                                <form style="margin-top: 15px;" action="/approve-bimbingan" method="POST">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="BimbinganID" value="{{ $bimbingan->ID }}">
                                    <input type="hidden" name="DosenID" value="{{ Auth::user()->username }}">
                                    <input type="submit" class="btn btn-default" value="Approve">
                                </form>
                            @elseif (($aprv == 1) AND (Auth::user()->username == $bimbingan->DosenID[$key]))
                                <b class="text-success">Anda sudah menyetujui</b>
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
